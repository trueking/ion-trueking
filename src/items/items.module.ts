import ItemsViewsModule from './views/items.views.module';
import ItemsServicesModule from "./services/items.services.module";

export default class ItemsModule {
    private ngItemsModule: angular.IModule;
    constructor(){
        this.ngItemsModule = angular.module(ItemsModule.moduleName(),[
            new ItemsServicesModule().getModule().name,
            new ItemsViewsModule().getModule().name
        ]);
    }

    public static moduleName():string {
        return 'trueking.items';
    }

    public getModule():angular.IModule {
        return this.ngItemsModule;
    }
}
