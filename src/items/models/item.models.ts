import {Self} from "../../common/models/common.models";
import {Profile} from "../../profiles/models/profile.models";

export class Item {

    public self:Self;
    public id:number;
    public name:string;
    public description:string;
    public imgs:Array<string>;
    public tags:Array<string>;
    public owner:number;
    public historic:boolean;
    public wishedTags:Array<string>;

    constructor(json?:any) {
        if (!json) json = {};
        this.self = json.self || new Self();
        this.id = json.id;
        this.name = json.name;
        this.description = json.description;
        this.tags = json.tags || [];
        this.imgs = json.imgs || [];
        this.owner = json.owner;
        this.historic = json.historic;
        this.wishedTags = json.wishedTags;
    }
}

export class ItemSummary {
    constructor(){}
}
