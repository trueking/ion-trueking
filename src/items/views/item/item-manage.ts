import {ItemsApiServiceModule,ItemsApiService} from "../../services/items-api.service";
import {Item} from "../../models/item.models";
import {PictureServiceModule, PictureService} from "../../../common/services/picture.service";
import {ToastService} from "../../../common/services/toast.service";
let ItemManageTpl = require('./item-manage.tpl.html');

export default class ItemManageViewModule {

    private ngItemManageViewModule:angular.IModule;

    constructor() {
        this.ngItemManageViewModule = angular.module(ItemManageViewModule.moduleName(), [
                ItemsApiServiceModule.moduleName(),
                PictureServiceModule.moduleName()
            ])
            .config(ItemManageConfig);
    }

    public static moduleName():string {
        return 'trueking.items.views.item-manage';
    }

    public getModule():angular.IModule {
        return this.ngItemManageViewModule;
    }
}

abstract class commonItemCtrl {
    public item:Item;

    constructor(protected $state:angular.ui.IStateService) {}

    public abstract submitItem():void;
}

abstract class ItemManageCtrl extends commonItemCtrl {

    constructor($state:angular.ui.IStateService, protected $ionicHistory:ionic.navigation.IonicHistoryService,
                protected $ionicPopup:ionic.popup.IonicPopupService, protected itemsApi:ItemsApiService,
                protected pictureService:PictureService, protected toastService:ToastService) {
        super($state);
    }

    public takePictureFromCamera():void {
        this.pictureService.takePictureFromCamera()
            .then((pictureUri)=> {
                this.item.imgs.push(pictureUri);
            });
    }

    public takePictureFromGallery():void {
        this.pictureService.takePictureFromGallery()
            .then((pictureUri)=> {
                this.item.imgs.push(pictureUri);
            });
    }

    public cancelCreation():void {
        this.$ionicHistory.goBack();
    }

}

abstract class EditItemCtrl extends ItemManageCtrl {
    public static $inject:Array<string> = ['$state', '$ionicHistory','$ionicPopup','itemsApi', 'pictureService','toastService','item'];

    constructor($state:angular.ui.IStateService, $ionicHistory:ionic.navigation.IonicHistoryService,
                $ionicPopup:ionic.popup.IonicPopupService, itemsApi:ItemsApiService,
                pictureService:PictureService, toastService:ToastService, item:Item) {
        super($state,$ionicHistory,$ionicPopup,itemsApi,pictureService,toastService);
        this.item = item;
    }

    public submitItem():void {
        this.itemsApi.update(this.item)
            .then((updatedItem:Item)=> {
                this.toastService.showShortBottom('Article actualitzat correctament');
                this.$ionicHistory.goBack();
            });
    }

    public deleteItem():void {
        let popupOptions:ionic.popup.IonicPopupConfirmOptions = {
            title: 'Esborrar article',
            subTitle: 'Aquest article no apareixerà a la búsqueda dels altres usuaris. ' +
                      'Està segur que vol esborrar aquest article de la seva llista d\'articles ofertats?',
            cancelText: 'No',
            okText: 'Si',
            okType: 'button-assertive'
        };

        this.$ionicPopup.confirm(popupOptions)
            .then((resAction:boolean)=>{
                if(resAction) {
                    this.itemsApi.delete(this.item)
                        .then(()=>{
                            this.$ionicHistory.goBack();
                        });
                }
            });
    }
}

class NewItemCtrl extends ItemManageCtrl {
    public static $inject:Array<string> = ['$state', '$ionicHistory','$ionicPopup','itemsApi', 'pictureService','toastService'];

    constructor($state:angular.ui.IStateService, $ionicHistory:ionic.navigation.IonicHistoryService,
                $ionicPopup:ionic.popup.IonicPopupService, itemsApi:ItemsApiService,
                pictureService:PictureService, toastService:ToastService) {
        super($state,$ionicHistory,$ionicPopup,itemsApi,pictureService,toastService);
        this.item = new Item();
    }

    public submitItem():void {
        this.itemsApi.create(this.item)
            .then((createdItem:Item)=> {
                this.toastService.showShortBottom('Article creat correctament');
                this.$ionicHistory.goBack();
            });
    }
}

class DetailItemCtrl extends commonItemCtrl{
    public static $inject:Array<string> = ['$state','item'];
    constructor($state:angular.ui.IStateService,item:Item){
        super($state);
        this.item = item;
    }

    public navigateToUserProfile():void {
        this.$state.go('profile',{userId:this.item.owner});
    }

    public submitItem():void {
        //TODO: link the views
        console.log('go to trueks view!');
    }
}

class ItemManageConfig {
    public static $inject:Array<string> = ['$stateProvider'];

    constructor($stateProvider:angular.ui.IStateProvider) {
        $stateProvider
            .state('newItem', {
                parent: 'home',
                url: '/items/new',
                templateUrl: ItemManageTpl,
                controller: NewItemCtrl,
                controllerAs: 'ctrl'
            })
            .state('editItem', {
                parent: 'home',
                url: '/items/:itemId/edit',
                templateUrl: ItemManageTpl,
                controller: EditItemCtrl,
                controllerAs: 'ctrl',
                resolve: {
                    item:['$stateParams','itemsApi',itemResolve]
                }
            })
            .state('viewItem', {
                parent: 'home',
                url: '/items/:itemId/detail',
                templateUrl: ItemManageTpl,
                controller: DetailItemCtrl,
                controllerAs: 'ctrl',
                resolve: {
                    item:['$stateParams','itemsApi',itemResolve]
                }
            })
    }
}

function itemResolve($stateParams:angular.ui.IStateParamsService,itemsApi:ItemsApiService):angular.IPromise<Item> {
    return itemsApi.query($stateParams['itemId']);
}
