import {ItemsApiService} from "../../services/items-api.service";
import {Item} from "../../models/item.models";
import {Profile} from "../../../profiles/models/profile.models";
let ItemsListTpl = require('./items-list.tpl.html');
let ImageNotFoundPicture = require('./../../../common/assets/pictures/image_not_found.jpg');
let ItemsSearchModalTpl = require('./items-search.tpl.html');

export default class ItemsListViewModule {

    private ngItemsListViewModule:angular.IModule;

    constructor() {
        this.ngItemsListViewModule = angular.module(ItemsListViewModule.moduleName(), [])
            .config(ItemsListConfig);
    }

    public static moduleName():string {
        return 'trueking.items.views.items-list';
    }

    public getModule():angular.IModule {
        return this.ngItemsListViewModule;
    }
}

class ItemSearch {
	public name:string;
	public haveTradeChance:boolean;
	public isNearMyLocation:boolean;
	public isInMyWishList:boolean;
	public tags:Array<string>;

	constructor() {
		this.tags = [];
	}
}

class ItemsListCtrl {
	public static $inject:Array<string> = ['$scope','$q','$ionicModal','itemsApi','myProfile','items'];
	public imageNotFoundPicture:any;
    public searchModal:ionic.modal.IonicModalController;
	public advancedSearch: ItemSearch;
	public quickNameSearch:string;

	constructor(private $scope:any, private $q:angular.IQService, private $ionicModal:ionic.modal.IonicModalService, private itemsApi:ItemsApiService,
				private myProfile:Profile, private items:Array<Item>) {
        this.imageNotFoundPicture = ImageNotFoundPicture;

        //Load search Modal
		let ionicModalOptions:ionic.modal.IonicModalOptions={
            scope: this.$scope,
            animation: 'slide-in-up'
        };
        this.$ionicModal.fromTemplateUrl(ItemsSearchModalTpl,ionicModalOptions)
            .then((modal:ionic.modal.IonicModalController)=>{
                this.searchModal = modal;
            });

		//Destroy detailModal and imagePopover when free list view
		this.$scope.$on('$destroy',()=> {
			if (this.searchModal) this.searchModal.remove();
		});

		//Search fields
		this.advancedSearch = new ItemSearch();
    }

    public showSearchModal():void{
        this.searchModal.show();
    }

	public hideSearchModal():void {
		this.searchModal.hide();
	}

	public searchItems():void{
		let itemsPromises:Array<angular.IPromise<Array<Item>>> = [];
		if (this.advancedSearch.name) itemsPromises.push(this.itemsApi.queryByName(this.advancedSearch.name,true));
		if (this.advancedSearch.haveTradeChance) itemsPromises.push(this.itemsApi.queryByOwnersWishList(true));
		if (this.advancedSearch.isNearMyLocation) itemsPromises.push(this.itemsApi.queryByZipCode(true));
		if (this.advancedSearch.isInMyWishList) itemsPromises.push(this.itemsApi.queryByMyWishList(true));
		if (this.advancedSearch.tags && this.advancedSearch.tags.length) itemsPromises.push(this.itemsApi.queryByTags(this.advancedSearch.tags,true));
		//Executing all service requests in parallel
		this.$q.all(itemsPromises)
			.then((itemsResultsArray:Array<Array<Item>>)=>{
				//Sorting arrays by length
				itemsResultsArray.sort((a, b)=> {
					return a.length - b.length;
				});

				//Return the array of intersected results
				return itemsResultsArray.shift().filter((v)=> {
					return itemsResultsArray.every((a)=> {
						for(let i = 0; i < a.length; ++i) {
							if (a[i].id === v.id) return true;
						}
						return false;
					});
				});
			})
			.then((itemsResult:Array<Item>)=>{
				this.items = itemsResult;
				this.searchModal.hide();
			});
	}

	public searchItemsByName():void{
		if (this.quickNameSearch) {
			this.itemsApi.queryByName(this.quickNameSearch)
				.then((items:Array<Item>)=>{
					this.items = items;
				});
		}
	}

    public doRefresh():void {
		this.quickNameSearch = null;
        this.itemsApi.queryAll(true,false)
        .then((items:Array<Item>)=>{
            this.items = items;
        })
        .finally(()=> {
            this.$scope.$broadcast('scroll.refreshComplete');
        });
    }
}

class ItemsListConfig {
    public static $inject:Array<string> = ['$stateProvider'];

    constructor($stateProvider:angular.ui.IStateProvider) {
        $stateProvider
            .state('items', {
                parent: 'home',
                url: '/items',
                templateUrl: ItemsListTpl,
                controller: ItemsListCtrl,
                controllerAs: 'ctrl',
                resolve: {
                    items:['itemsApi',(itemsApi:ItemsApiService)=>itemsApi.queryAll()]
                }
            })
    }
}
