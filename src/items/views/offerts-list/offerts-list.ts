import {Item} from "../../models/item.models";
import {ItemsApiService} from "../../services/items-api.service";
import {ProfilesApiService} from "../../../profiles/services/profiles-api.service";
import {Profile} from "../../../profiles/models/profile.models";
let OffertsListTpl = require('./offerts-list.tpl.html');

export default class OffertsListViewModule {

    private ngOffertsListViewModule:angular.IModule;

    constructor() {
        this.ngOffertsListViewModule = angular.module(OffertsListViewModule.moduleName(), [])
            .config(OffertsListConfig);
    }

    public static moduleName():string {
        return 'trueking.items.views.offerts-list';
    }

    public getModule():angular.IModule {
        return this.ngOffertsListViewModule;
    }
}

abstract class OffertsListCtrl {
    protected canEditOfferts:boolean;
    protected currentItems: Array<Item>;
    protected historicItems: Array<Item>;
    constructor(protected items:Array<Item>) {
        this.currentItems = [];
        this.historicItems = [];
        for (let i = 0; i < this.items.length; ++i) {
            if (items[i].historic) this.historicItems.push(items[i]);
            else this.currentItems.push(items[i]);
        }
    }
}

class MyOffertsListCtrl extends OffertsListCtrl {
    public static $inject:Array<string> = ['myItems', 'myProfile'];
    constructor(myItems:Array<Item>, protected myProfile:Profile) {
        super(myItems);
        this.canEditOfferts = true;
    }
}

class UserOffertsListCtrl extends OffertsListCtrl {
    public static $inject:Array<string> = ['items', 'myProfile'];
    constructor(items:Array<Item>, protected myProfile:Profile) {
        super(items);
        this.canEditOfferts = false;
    }
}

class OffertsListConfig {
    public static $inject:Array<string> = ['$stateProvider'];

    constructor($stateProvider:angular.ui.IStateProvider) {
        $stateProvider
            .state('myOfferts', {
                parent: 'home',
                url: '/myOfferts',
                templateUrl: OffertsListTpl,
                controller: MyOffertsListCtrl,
                controllerAs: 'ctrl',
                resolve: {
                    myItems:['itemsApi',myItemsResolve]
                }
            })
            .state('offerts', {
                parent: 'home',
                url: '/profiles/:userId/offerts',
                templateUrl: OffertsListTpl,
                controller: UserOffertsListCtrl,
                controllerAs: 'ctrl',
                resolve:{
                    items:['$stateParams','profilesApi',userItemsResolve]
                }
            });
    }
}

function myItemsResolve(itemsApi:ItemsApiService):ng.IPromise<Array<Item>> {
    return itemsApi.queryMine();
}

function userItemsResolve($stateParams:angular.ui.IStateParamsService, profilesApi:ProfilesApiService):ng.IPromise<Array<Item>> {
    return profilesApi.getUserItems($stateParams['userId']);
}