import ItemsListViewModule from "./items-list/items-list";
import ItemManageViewModule from "./item/item-manage";
import OffertsListViewModule from "./offerts-list/offerts-list";

export default class ItemsViewsModule {
    private ngItemsViewsModule:angular.IModule;

    constructor() {
        this.ngItemsViewsModule = angular.module(ItemsViewsModule.moduleName(), [
            new ItemsListViewModule().getModule().name,
            new ItemManageViewModule().getModule().name,
            new OffertsListViewModule().getModule().name
        ]);
    }

    public static moduleName():string {
        return 'trueking.items.views';
    }

    public getModule():angular.IModule {
        return this.ngItemsViewsModule;
    }
}