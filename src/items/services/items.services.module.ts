import {ItemsApiServiceModule} from "./items-api.service";

export default class ItemsServicesModule {

    private ngItemsServicesModule:angular.IModule;

    constructor() {
        this.ngItemsServicesModule = angular.module(ItemsServicesModule.moduleName(), [
            new ItemsApiServiceModule().getModule().name
        ]);
    }

    public static moduleName():string {
        return 'trueking.items.services';
    }

    public getModule():angular.IModule {
        return this.ngItemsServicesModule;
    }
}