import {RestApiServiceModule,RestApiService} from "../../common/services/rest-api.service";
import {Item} from "../models/item.models";

export class ItemsApiServiceModule {
    private ngItemsApiServiceModule:angular.IModule;

    constructor() {
        this.ngItemsApiServiceModule = angular.module(ItemsApiServiceModule.moduleName(), [
                RestApiServiceModule.moduleName()
            ])
            .service('itemsApi', ItemsApiService);
    };

    public static moduleName():string {
        return 'trueking.items.services.items-api';
    }

    public getModule():angular.IModule {
        return this.ngItemsApiServiceModule;
    }

}

export class ItemsApiService {
    public static $inject = ['$q','restApi'];
    public itemsPath:string = '/items';

    constructor(private $q:angular.IQService, private restApi:RestApiService) {}

    public queryAll(avoidCache:boolean=false, showLoading:boolean = true):angular.IPromise<Array<Item>> {
        return this.restApi.get(this.itemsPath,null,null,null,avoidCache,showLoading)
            .then((respData:any)=>{
                return respData.data;
            });
    }

    public query(itemId):angular.IPromise<Item> {
        return this.restApi.get(this.itemsPath + '/' + itemId)
            .then((respData:any)=>{
                return respData.data;
            });
    }

    public queryMine():ng.IPromise<Array<Item>> {
        return this.restApi.get(this.itemsPath + '/myItems')
            .then((respData:any)=>{
                return respData.data;
            });
    }

	public queryByName(itemName:string, avoidCache:boolean=false, showLoading:boolean = true):angular.IPromise<Array<Item>> {
		return this.restApi.get(this.itemsPath +'/search/' + itemName,null,null,null,avoidCache,showLoading)
            .then((resp:any)=>{
                return resp.data;
            });
	}

	public queryByZipCode(avoidCache:boolean=false, showLoading:boolean = true):angular.IPromise<Array<Item>> {
		return this.restApi.get(this.itemsPath + '/search/zipCode',null,null,null,avoidCache,showLoading)
            .then((resp:any)=>{
                return resp.data;
            });
	}


	public queryByOwnersWishList(avoidCache:boolean=false, showLoading:boolean = true):angular.IPromise<Array<Item>> {
		return this.restApi.get(this.itemsPath + '/search/byOwnersWishList',null,null,null,avoidCache,showLoading)
            .then((resp:any)=>{
                return resp.data;
            });
	}

    public queryByMyWishList(avoidCache:boolean=false, showLoading:boolean = true):angular.IPromise<Array<Item>> {
        return this.restApi.get(this.itemsPath + '/search/somethingIwish',null,null,null,avoidCache,showLoading)
            .then((resp:any)=>{
                return resp.data;
            });
    }

	public queryByTags(itemsTags:Array<String>, avoidCache:boolean=false, showLoading:boolean = true):angular.IPromise<Array<any>> {
		return this.restApi.get(this.itemsPath + '/search',{tags: itemsTags.join(',')},null,null,avoidCache,showLoading)
			.then((respData:any)=>{
				return respData.data;
			});
	}

    public create(item:Item):angular.IPromise<any> {
        return this.restApi.post(this.itemsPath,item)
            .then((respData:any)=>{
                this.restApi.invalidateUriIgnoreParams(this.itemsPath + '/myItems');
                return respData.data;
            });
    }

    public update(item:Item):angular.IPromise<any> {
        return this.restApi.put(this.itemsPath + '/' + item.id, item)
            .then((respData:any)=>{
                this.restApi.invalidateUri(this.itemsPath + '/' + item.id);
                this.restApi.invalidateUriIgnoreParams(this.itemsPath + '/myItems');
                return respData.data;
            });
    }

    public delete(item:Item):angular.IPromise<any> {
        return this.restApi.delete(this.itemsPath + '/' + item.id)
            .then((respData:any)=>{
                this.restApi.invalidateUri(this.itemsPath + '/' + item.id);
                this.restApi.invalidateUriIgnoreParams(this.itemsPath + '/myItems');
                return respData.data;
            });
    }
}