//Global custom styles
import'./common/css/custom.css';
//Angular ecosystem
import MainModule from './main.module';

class Main {
    constructor(){}
    public static main():void {
        new MainModule();
    }
}

Main.main();
