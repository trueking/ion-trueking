import {Self} from "../../common/models/common.models";
import {Profile} from "../../profiles/models/profile.models";
import {Item} from "../../items/models/item.models";

export class TruekingGroup {
	public id:number;
	public active:boolean;
	public accepted:boolean;

	constructor(json?:any) {
		if (!json) json = {};
		this.id = json.id;
		this.active = json.active;
		this.accepted = json.accepted;
	}
}

export class TruekingPart {
	public usedID:number;
	public acceptance:boolean;
	public items:Array<Item>;

	constructor(json?:any) {
		if (!json) json = {};
		this.usedID = json.id;
		this.acceptance = json.acceptance;
		this.items = json.items || [];
	}
}

export class Trueking {
	public id:number;
	public truekingGroup:TruekingGroup;
	public selfPart:TruekingPart;
	public otherPart:TruekingPart;
	public active:boolean;
	public accepted:boolean;

	constructor(json?:any) {
		if (!json) json = {};
		this.id = json.id;
		this.truekingGroup = json.truekingGroup || new TruekingGroup();
		this.selfPart = json.selfPart || new TruekingPart();
		this.otherPart = json.otherPart ||new TruekingPart();
		this.active = json.active;
		this.accepted = json.accepted;
	}
}
