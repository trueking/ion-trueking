import {RestApiServiceModule,RestApiService} from "../../common/services/rest-api.service";
import {Trueking} from "../models/trueking.models";

export class TruekingsApiServiceModule {
	private ngTruekingsApiServiceModule:angular.IModule;

	constructor() {
		this.ngTruekingsApiServiceModule = angular.module(TruekingsApiServiceModule.moduleName(), [
				RestApiServiceModule.moduleName()
			])
			.service('truekingsApi', TruekingsApiService);
	};

	public static moduleName():string {
		return 'trueking.truekings.services.truekings-api';
	}

	public getModule():angular.IModule {
		return this.ngTruekingsApiServiceModule;
	}

}

export class TruekingsApiService {
	public static $inject = ['$q','restApi'];
	public truekingsPath:string = '/trueques';

	constructor(private $q:angular.IQService, private restApi:RestApiService) {}


	public create(trueking:Trueking):angular.IPromise<any> {
		return this.restApi.post(this.truekingsPath,trueking)
			.then((respData:any)=>{
				this.restApi.invalidateUriAndSubUris(this.truekingsPath);
				return respData.data;
			});
	}

	public queryMine(avoidCache:boolean=false, showLoading:boolean=true):angular.IPromise<Array<Trueking>> {
		return this.restApi.get(this.truekingsPath,null,null,null,avoidCache,showLoading)
			.then((resp:any)=>{
				return resp.data;
			})
	}

	public query(truekingGroupId,truekingId):angular.IPromise<Trueking> {
		return this.restApi.get(this.truekingsPath + '/' + truekingGroupId + '/' + truekingId)
			.then((resp:any)=>{
				return resp.data;
			})
	}

	public update(trueking:Trueking):angular.IPromise<any> {
		return this.restApi.put(this.truekingsPath + '/' + trueking.truekingGroup.id + '/' + trueking.id,trueking)
			.then((respData:any)=>{
				this.restApi.invalidateUriAndSubUris(this.truekingsPath);
				return respData.data;
			});
	}
}
