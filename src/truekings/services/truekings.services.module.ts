import {TruekingsApiServiceModule} from "./truekings-api.services";

export default class TruekingsServicesModule {

	private ngTruekingsServicesModule:angular.IModule;

	constructor() {
		this.ngTruekingsServicesModule = angular.module(TruekingsServicesModule.moduleName(), [
			new TruekingsApiServiceModule().getModule().name
		]);
	}

	public static moduleName():string {
		return 'trueking.truekings.services';
	}

	public getModule():angular.IModule {
		return this.ngTruekingsServicesModule;
	}
}
