import TruekingsViewsModule from "./views/truekings.views.module";
import TruekingsServicesModule from "./services/truekings.services.module";

export default class TruekingsModule {
	private ngTruekingsModule: angular.IModule;
	constructor(){
		this.ngTruekingsModule = angular.module(TruekingsModule.moduleName(),[
			new TruekingsServicesModule().getModule().name,
			new TruekingsViewsModule().getModule().name
		]);
	}

	public static moduleName():string {
		return 'trueking.truekings';
	}

	public getModule():angular.IModule {
		return this.ngTruekingsModule;
	}
}
