import TruekingManageViewModule from "./trueking-manage/trueking-manage";
import './../css/truekings.css';
import TruekingsListViewModule from "./truekings-list/truekings-list";

export default class TruekingsViewsModule {
	private ngTruekingsViewsModule:angular.IModule;

	constructor() {
		this.ngTruekingsViewsModule = angular.module(TruekingsViewsModule.moduleName(), [
			new TruekingManageViewModule().getModule().name,
			new TruekingsListViewModule().getModule().name
		]);
	}

	public static moduleName():string {
		return 'trueking.truekings.views';
	}

	public getModule():angular.IModule {
		return this.ngTruekingsViewsModule;
	}
}
