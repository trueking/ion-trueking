import {ItemsApiService} from "../../../items/services/items-api.service.ts";
import {Item} from "../../../items/models/item.models.ts";
import {Trueking} from "../../models/trueking.models.ts";
import {TruekingsApiServiceModule, TruekingsApiService} from "../../services/truekings-api.services.ts";
import {Profile} from "../../../profiles/models/profile.models";
import {ToastService} from "../../../common/services/toast.service";
let TruekingManageTpl = require('./trueking-manage.tpl.html');
let AvailableItemsModalTpl = require('./available-items-modal.tpl.html');

export default class TruekingManageViewModule {

	private ngTruekingManageViewModule:angular.IModule;

	constructor() {
		this.ngTruekingManageViewModule = angular.module(TruekingManageViewModule.moduleName(), [
				TruekingsApiServiceModule.moduleName()
			])
			.config(TruekingManageConfig);
	}

	public static moduleName():string {
		return 'trueking.truekings.views.trueking-manage';
	}

	public getModule():angular.IModule {
		return this.ngTruekingManageViewModule;
	}
}

abstract class TruekingManageCtrl {
	public trueking:Trueking;
	public availableItemsModal:any;
	public availableItems:Array<Item>;

	constructor(protected $scope:angular.IScope, protected $state:angular.ui.IStateService, protected $ionicModal:ionic.modal.IonicModalService,
				protected $ionicHistory:ionic.navigation.IonicHistoryService, protected $ionicPopup:ionic.popup.IonicPopupService,
				protected truekingsApi:TruekingsApiService, protected toastService:ToastService, protected myItems:Array<Item>, protected myProfile:Profile) {
		//Load search Modal
		let ionicModalOptions:ionic.modal.IonicModalOptions={
			scope: this.$scope,
			animation: 'slide-in-up'
		};
		this.$ionicModal.fromTemplateUrl(AvailableItemsModalTpl,ionicModalOptions)
			.then((modal:ionic.modal.IonicModalController)=>{
				this.availableItemsModal = modal;
			});

		//Destroy detailModal and imagePopover when free list view
		this.$scope.$on('$destroy',()=> {
			if (this.availableItemsModal) this.availableItemsModal.remove();
		});
	}

	public showAvailableItemsModal():void {
		this.availableItems = this.myItems.filter((item:Item)=>{
			return this.trueking.selfPart.items.indexOf(item) === -1;
		});

		this.availableItemsModal.show();
	}

	public addOffertedItems():void {
		let selectedItems:Array<Item> = this.availableItems.filter((item:any)=>{
			let isSelected:boolean = item.selected;
			delete item.selected;
			return isSelected;
		});
		this.trueking.selfPart.items = this.trueking.selfPart.items.concat(selectedItems);
		this.availableItemsModal.hide();
	}

	public removeOffertedItems():void {
		let notSelectedItems:Array<Item>;
		notSelectedItems = this.trueking.selfPart.items.filter((item:any)=>{
			let isSelected:boolean = item.selected;
			delete item.selected;
			return !isSelected;
		});
		this.trueking.selfPart.items = notSelectedItems;
	}

	public abstract submitTrueking():void;

}

class TruekingEditCtrl extends TruekingManageCtrl {
	public static $inject:Array<string> = ['$scope','$state','$ionicModal','$ionicHistory','$ionicPopup','truekingsApi','toastService','myItems','myProfile','trueking'];

	constructor($scope:angular.IScope, $state:angular.ui.IStateService, $ionicModal:ionic.modal.IonicModalService,
				$ionicHistory:ionic.navigation.IonicHistoryService, $ionicPopup:ionic.popup.IonicPopupService, truekingsApi:TruekingsApiService,
				toastService:ToastService, myItems:Array<Item>, myProfile:Profile, trueking:Trueking) {
		super($scope,$state,$ionicModal,$ionicHistory,$ionicPopup,truekingsApi,toastService,myItems,myProfile);

		this.trueking = trueking;
	}

	public submitTrueking():void {
		let popupOptions:ionic.popup.IonicPopupConfirmOptions = {
			title: 'Modificar trueking',
			subTitle: 'Està segur que vol modificar aquesta proposta de trueking?',
			cancelText: 'No',
			okText: 'Si'
		};

		this.$ionicPopup.confirm(popupOptions)
			.then((resAction:boolean)=>{
				if(resAction){
					this.truekingsApi.update(this.trueking)
						.then(()=>{
							this.toastService.showShortBottom('Trueking modificat correctament');
							this.$ionicHistory.nextViewOptions({disableBack:true});
							this.$state.go('myTruekings');
						});
				}
			});
	}
}

class TruekingNewCtrl extends TruekingManageCtrl {
	public static $inject:Array<string> = ['$scope','$state', '$ionicModal', '$ionicHistory', '$ionicPopup','truekingsApi','toastService','wishedItem','myItems','myProfile'];
	constructor($scope:angular.IScope, $state:angular.ui.IStateService,$ionicModal:ionic.modal.IonicModalService, $ionicHistory:ionic.navigation.IonicHistoryService,
				$ionicPopup:ionic.popup.IonicPopupService, truekingsApi:TruekingsApiService, toastService:ToastService,
				private wishedItem:Item, myItems:Array<Item>, myProfile:Profile) {
		super($scope,$state, $ionicModal, $ionicHistory,$ionicPopup, truekingsApi, toastService,myItems,myProfile);
		this.trueking = new Trueking();
		this.trueking.id = 0;
		this.trueking.selfPart.usedID = this.myProfile.id;
		this.trueking.otherPart.usedID = this.wishedItem.owner;
		this.trueking.otherPart.items.push(this.wishedItem);
		this.availableItems = [];
	}

	public submitTrueking():void {
		let popupOptions:ionic.popup.IonicPopupConfirmOptions = {
			title: 'Enviar trueking',
			subTitle: 'Està segur que vol obrir aquesta proposta de trueking?',
			cancelText: 'No',
			okText: 'Si'
		};

		this.$ionicPopup.confirm(popupOptions)
			.then((resAction:boolean)=>{
				if(resAction){
					this.truekingsApi.create(this.trueking)
						.then(()=>{
							this.toastService.showShortBottom('Trueking creat correctament');
							this.$ionicHistory.nextViewOptions({disableBack:true});
							this.$state.go('myTruekings');
						});
				}
			});
	}
}

class TruekingManageConfig {
	public static $inject:Array<string> = ['$stateProvider'];
	constructor($stateProvider:angular.ui.IStateProvider) {
		$stateProvider
			.state('newTrueking', {
				parent: 'home',
				url: '/truekings/new?wishedItem',
				templateUrl: TruekingManageTpl,
				controller: TruekingNewCtrl,
				controllerAs: 'ctrl',
				resolve: {
					myItems:['itemsApi',myItemsResolve],
					wishedItem:['$stateParams','itemsApi',itemResolve]
				}
			})
			.state('editTrueking', {
			parent: 'home',
			url: '/truekings/:truekingGroupId/:truekingId/edit',
			templateUrl: TruekingManageTpl,
			controller: TruekingEditCtrl,
			controllerAs: 'ctrl',
			resolve: {
				trueking:['$stateParams','truekingsApi',truekingResolve]
			}
		});
	}
}

function truekingResolve($stateParams:angular.ui.IStateParamsService,truekingsApi:TruekingsApiService) {
	return truekingsApi.query($stateParams['truekingGroupId'],$stateParams['truekingId']);
}

function myItemsResolve(itemsApi:ItemsApiService) {
	return itemsApi.queryMine()
		.then((myItems:Array<Item>)=>{
			return myItems.filter((item:Item)=>{
				return !item.historic;
			});
		});
}

function itemResolve($stateParams:angular.ui.IStateParamsService,itemsApi:ItemsApiService):angular.IPromise<Item> {
	return itemsApi.query($stateParams['wishedItem']);
}
