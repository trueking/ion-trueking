import {TruekingsApiService} from "../../services/truekings-api.services";
import {Trueking} from "../../models/trueking.models";
let TruekingsListTpl = require('./truekings-list.tpl.html');

export default class TruekingsListViewModule {

	private ngTruekingsListViewModule:angular.IModule;

	constructor() {
		this.ngTruekingsListViewModule = angular.module(TruekingsListViewModule.moduleName(), [])
			.config(TruekingsListConfig);
	}

	public static moduleName():string {
		return 'trueking.truekings.views.truekings-list';
	}

	public getModule():angular.IModule {
		return this.ngTruekingsListViewModule;
	}
}

class TruekingsListCtrl {
	public static $inject:Array<string> = ['$scope','$state','truekingsApi','myTruekings'];
	constructor(private $scope:angular.IScope,private $state:angular.ui.IStateService, private truekingsApi:TruekingsApiService, private myTruekings:Array<Trueking>) {}

	public navigateToTrueking(trueking:Trueking):void {
		this.$state.go('editTrueking',{
			truekingGroupId:trueking.truekingGroup.id,
			truekingId:trueking.id
		});
	}

	public doRefresh():void {
		this.truekingsApi.queryMine(true,false)
			.then((truekings:Array<Trueking>)=>{
				this.myTruekings = truekings;
			})
			.finally(()=> {
				this.$scope.$broadcast('scroll.refreshComplete');
			});
	}
}

class TruekingsListConfig {
	public static $inject:Array<string> = ['$stateProvider'];

	constructor($stateProvider:angular.ui.IStateProvider) {
		$stateProvider
			.state('myTruekings', {
				parent: 'home',
				url: '/truekings/mine',
				templateUrl: TruekingsListTpl,
				controller: TruekingsListCtrl,
				controllerAs: 'ctrl',
				resolve: {
					myTruekings:['truekingsApi',(truekingsApi:TruekingsApiService)=>truekingsApi.queryMine()]
				}
			})
	}
}
