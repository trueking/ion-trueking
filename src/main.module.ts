import CommonServicesModule from './common/services/common.services.module';
import CommonComponentsModule from './common/components/common.components.module';
import CommonUtilsModule from "./common/utils/common.utils.module";
import LoginModule from "./login/login.module";
import HomeModule from './home/home.module';
import ItemsModule from './items/items.module';
import ProfilesModule from "./profiles/profiles.module";
import {LocalStorageService} from "./common/services/localstorage.service";
import {LoginService} from "./login/services/login.service";
import TruekingsModule from "./truekings/truekings.module";

export default class MainModule {
    private ngMainModule:angular.IModule;

    constructor() {
        this.ngMainModule = angular.module(MainModule.moduleName(), [
            //Global vendors
            'ionic',
            new CommonServicesModule().getModule().name,
            new CommonComponentsModule().getModule().name,
            new CommonUtilsModule().getModule().name,
            new LoginModule().getModule().name,
            new HomeModule().getModule().name,
            new ItemsModule().getModule().name,
            new ProfilesModule().getModule().name,
			new TruekingsModule().getModule().name
        ]);
        this.ngMainModule
            .config(MainConfig)
            .run(MainRun);
    }

    public static moduleName():string {
        return 'trueking.main';
    }
}

class MainConfig {
    public static $inject = ['$urlRouterProvider'];

    constructor($urlRouterProvider:angular.ui.IUrlRouterProvider) {
        $urlRouterProvider.otherwise(($injector:any)=> {
            let localStorageService:LocalStorageService = $injector.get('localStorageService');
            let $state:angular.ui.IStateService = $injector.get('$state');
            if (localStorageService.get('credentials-token')) {
                let loginService:LoginService = $injector.get('loginService');
                loginService.login()
                    .then((accepted:boolean)=> {
                        if (!accepted) $state.go('loginWelcome');
                    },(errResp:any)=> {
                        $state.go('loginWelcome');
                    });
            } else {
				$state.go('loginWelcome');
			}
        });
    }
}

class MainRun {
    public static $inject = ['$ionicPlatform','$ionicLoading'];


    constructor($ionicPlatform:ionic.platform.IonicPlatformService,$ionicLoading:ionic.loading.IonicLoadingService) {
        //$ionicLoading.show();
        /*$ionicPlatform.ready(()=> {
         if (window.cordova && window.cordova.plugins.Keyboard) {
         // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
         // for form inputs)
         cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

         // Don't remove this line unless you know what you are doing. It stops the viewport
         // from snapping when text inputs are focused. Ionic handles this internally for
         // a much nicer keyboard experience.
         cordova.plugins.Keyboard.disableScroll(true);
         }
         if (window.StatusBar) {
         StatusBar.styleDefault();
         }
         })*/
    }
}
