import {ItemSummary} from "../../items/models/item.models";
import {Self} from "../../common/models/common.models";
import {Credentials} from "../../common/models/common.models";
import {UserAccountInfo} from "../../login/models/account.models";

export class Profile extends UserAccountInfo{
    public id:number;
    public profilePicture:string;
    public wishedTags:Array<string>;

    constructor(json?:any) {
        super(json);
        this.id = json.id;
        this.profilePicture = json.profilePicture;
        this.wishedTags = json.wishedTags || [];
    }
}
