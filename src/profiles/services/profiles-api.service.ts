import {RestApiService} from "../../common/services/rest-api.service";
import {Profile} from "../models/profile.models";
import {Item} from "../../items/models/item.models";

export class ProfilesApiServiceModule {
    private ngProfilesApiServiceModulee:angular.IModule;

    constructor() {
        this.ngProfilesApiServiceModulee = angular.module(ProfilesApiServiceModule.moduleName(), [
            ])
            .service('profilesApi', ProfilesApiService);
    };

    public static moduleName():string {
        return 'trueking.profiles.services.profiles-api';
    }

    public getModule():angular.IModule {
        return this.ngProfilesApiServiceModulee;
    }

}

export class ProfilesApiService {
    public static $inject = ['restApi'];
    public profilesEndpoint:string;
    constructor(private restApi:RestApiService) {
        this.profilesEndpoint = '/profiles';
    }

    public query(profileId:any):angular.IPromise<Profile> {
        return this.restApi.get(this.profilesEndpoint + '/' + profileId)
            .then((resp:any)=>{
                return new Profile(resp.data);
            });
    }

    public queryMine():angular.IPromise<Profile> {
        return this.query('mine');
    }

    public update(profile:Profile):angular.IPromise<any> {
        return this.restApi.put(this.profilesEndpoint + '/mine', profile)
            .then((resp:any)=> {
                this.restApi.invalidateUriIgnoreParams(this.profilesEndpoint + '/mine');
                return resp.data;
            });
    }

    public delete():angular.IPromise<any> {
        return this.restApi.delete(this.profilesEndpoint + '/mine')
            .then((resp:any)=>{
                this.restApi.invalidateUriIgnoreParams(this.profilesEndpoint + '/mine');
            });
    }

    public updateMyTags(tags:Array<string>):ng.IPromise<any> {
        return this.restApi.put(this.profilesEndpoint + '/mine/wishlist',{tags:tags})
            .then((resp:any)=>{
                this.restApi.invalidateUriAndSubUris(this.profilesEndpoint + '/mine');
                return resp.data;
            });
    }

    public getUserItems(userId:number):ng.IPromise<Array<Item>> {
        return this.restApi.get(this.profilesEndpoint + '/' + userId + '/offers')
            .then((respData)=>{
                return respData.data;
            });
    }

    public changePassword(newPassword:string):angular.IPromise<any> {
        return this.restApi.put(this.profilesEndpoint + '/mine/changePassword',newPassword);
    }
}