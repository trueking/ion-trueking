import {ProfilesApiServiceModule} from "./profiles-api.service";

export default class ProfilesServicesModule {

    private ngProfilesServicesModule:angular.IModule;

    constructor() {
        this.ngProfilesServicesModule = angular.module(ProfilesServicesModule.moduleName(), [
            new ProfilesApiServiceModule().getModule().name
        ]);
    }

    public static moduleName():string {
        return 'trueking.profiles.services';
    }

    public getModule():angular.IModule {
        return this.ngProfilesServicesModule;
    }
}