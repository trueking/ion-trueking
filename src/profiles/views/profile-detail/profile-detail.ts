import {Profile} from "../../models/profile.models";
import {LoginApiService} from "../../../login/services/login-api.service";
import {LoginService} from "../../../login/services/login.service";
import {PictureService} from "../../../common/services/picture.service";
import {LocalStorageService} from "../../../common/services/localstorage.service";
import {ProfilesApiService} from "../../services/profiles-api.service";
let ProfileDetailTpl = require('./profile-detail.tpl.html');
let UploadImageMenuPopover = require('./upload-image-menu-popover.tpl.html');
let ChangePasswordPopupTpl = require('./change-password-popup.tpl.html');
let ImageNotFoundPicture = require('./../../../common/assets/pictures/image_not_found.jpg');
let CryptoJS = require("./../../../../bower_components/crypto-js/crypto-js.js");

export default class ProfileDetailViewModule {

    private ngProfileDetailViewModule:angular.IModule;

    constructor() {
        this.ngProfileDetailViewModule = angular.module(ProfileDetailViewModule.moduleName(), [])
            .config(ProfileDetailConfig);
    }

    public static moduleName():string {
        return 'trueking.profiles.views.profile-detail';
    }

    public getModule():angular.IModule {
        return this.ngProfileDetailViewModule;
    }
}

abstract class ProfileDetailCtrl {
    public imageNotFoundPicture:any;
    public canEditProfile:boolean;
    public oldProfileMail:string;
    public oldProfileUserName:string;
    public changePasswordForm:any;
    public newPassword:string;
    public repeatNewPassword:string;

    constructor(protected $q:angular.IQService, protected loginApi:LoginApiService, public profile:Profile) {
        this.imageNotFoundPicture = ImageNotFoundPicture;
        this.oldProfileMail = this.profile.email;
        this.oldProfileUserName = this.profile.credentials.userName;
    }

    public profileChangeMailAsincValidator(email:string):angular.IPromise<any> {
        if (this.oldProfileMail == email) return this.$q.resolve();
        else return this.loginApi.checkEmailAvailable(email);
    }

    public profileChangeUserNameAsincValidator(userName:string):angular.IPromise<any> {
        if (this.oldProfileUserName == userName) return this.$q.resolve();
        else return this.loginApi.checkUserNameAvailable(userName);
    }
}

class MyProfileDetailCtrl extends ProfileDetailCtrl{
    public static $inject:Array<string> = ['$scope','$q','$state','$ionicPopover','$ionicPopup', 'loginApi','loginService','pictureService','localStorageService','profilesApi','myProfile'];
    public editingGeneralInfo:boolean;
    public uploadImageMenuPopover:ionic.popover.IonicPopoverController;

    constructor(private $scope:any, $q:angular.IQService, private $state:angular.ui.IStateService, private $ionicPopover:ionic.popover.IonicPopoverService,
                private $ionicPopup:ionic.popup.IonicPopupService, loginApi:LoginApiService,private loginService:LoginService, private pictureService:PictureService,
                private localStorageService:LocalStorageService, private profilesApi:ProfilesApiService, myProfile:Profile) {
        super($q, loginApi, myProfile);
        this.canEditProfile = true;
        this.editingGeneralInfo = false;

        //Image Menu popover
        this.$ionicPopover.fromTemplateUrl(UploadImageMenuPopover, {
           scope:this.$scope
        }).then((popover:any)=>{
            this.uploadImageMenuPopover = popover;
        });

        this.$scope.$on('$destroy',()=> {
            if (this.uploadImageMenuPopover) this.uploadImageMenuPopover.remove();
        });
    }

    public takeProfilePicture($event:angular.IAngularEvent):void {
        this.uploadImageMenuPopover.show($event);
    }

    public takePictureFromCamera():void {
        this.pictureService.takePictureFromCamera()
            .then((pictureUri)=> {
                this.profile.profilePicture = pictureUri;
                this.saveProfile();
            });
        this.uploadImageMenuPopover.hide();
    }

    public takePictureFromGallery():void {
        this.pictureService.takePictureFromGallery()
            .then((pictureUri)=> {
                this.profile.profilePicture = pictureUri;
                this.saveProfile();
            });
        this.uploadImageMenuPopover.hide();
    }

    public saveProfile():void {
        this.profilesApi.update(this.profile)
            .then(()=>{
                this.$state.reload();
            },()=> {
                this.$state.reload();
            });
    }

    public cancelEdit():void {
        this.$state.reload();
    }

    public changePasswordPopup():void {
        this.newPassword = '';
        this.repeatNewPassword ='';
        let popupOptions:any = {
            title: 'Modificar contrasenya',
            subTitle: 'Canvía la contrasenya d\'accès al teu compte trueking',
            templateUrl: ChangePasswordPopupTpl,
            scope: this.$scope,
            buttons:[
                {
                    text:'Cancel·lar',
                    type:'button-default'
                },
                {
                    text:'Ok',
                    type:'button-positive',
                    onTap:(e:any)=>{
                        if (!this.changePasswordForm.$valid) {
                            e.preventDefault();
                        } else {
                            this.changePassword(this.newPassword);
                        }
                    }
                }
            ]
        };
        this.$ionicPopup.show(popupOptions);
    }

    public changePassword(newPassword:string):void{
        let newPasswordMD5:string = CryptoJS.MD5(newPassword).toString();
        this.profilesApi.changePassword(newPasswordMD5)
            .then(()=>{
                let popupOptions:ionic.popup.IonicPopupAlertOptions = {
                    title: 'Contrasenya modificada correctament',
                    subTitle: 'Sisplau, torni a entrar al seu compte trueking',
                    okType: 'button-balanced'
                };
                this.$ionicPopup.alert(popupOptions)
                    .then(()=>{
                        this.loginService.logout();
                    });
            });
    }

    public logout():void {
        let popupOptions:ionic.popup.IonicPopupConfirmOptions = {
            title: 'Logout',
            subTitle: 'Està segur que vol sortir del seu compte?',
            cancelText: 'No',
            okText: 'Si'
        };

        this.$ionicPopup.confirm(popupOptions)
            .then((resAction:boolean)=>{
                if(resAction) this.loginService.logout();
            });
    }

    public deleteAccount():void {
        let popupOptions:ionic.popup.IonicPopupPromptOptions = {
            title:'Borrar compte',
            subTitle:'Si confirma aquesta acció s\'esborraràn totes les dades del seu perfil així com la seva activitat dintre de trueking. L\'efecte d\'aquesta acció no es recuperable.',
            cancelText:'Cancel·lar',
            okText: 'Donar-me de baixa',
            okType: 'button-assertive',
            inputType: 'password',
            inputPlaceholder:'Contrasenya del seu compte'
        };
        this.$ionicPopup.prompt(popupOptions)
            .then((password:string)=> {
                if (password) {
                    let passwordMD5:string = CryptoJS.MD5(password).toString();
                    let authToken:string = this.localStorageService.get('credentials-token');
                    let currentPasswordMD5:string = atob(authToken).split(':')[1];
                    if (currentPasswordMD5 === passwordMD5) {
                        this.profilesApi.delete()
                            .then(()=>{
                                let popupOptions:ionic.popup.IonicPopupAlertOptions = {
                                    title:'Fins aviat!',
                                    subTitle:'Gràcies per haver confiat en trueking'
                                };
                                this.$ionicPopup.alert(popupOptions)
                                    .then(()=>{
                                        this.loginService.logout();
                                    });
                            });
                    } else {
                        let popupOptions:ionic.popup.IonicPopupAlertOptions = {
                            title: 'Contrasenya incorrecte',
                            okType: 'button-assertive'
                        };
                        this.$ionicPopup.alert(popupOptions);
                    }
                }
            });
    }
}

class UserProfileDetailCtrl extends ProfileDetailCtrl{
    public static $inject:Array<string> = ['$q','$state','$stateParams','loginApi','profile'];
    constructor($q:angular.IQService, private $state:angular.ui.IStateService, private $stateParams:angular.ui.IStateParamsService,
                loginApi:LoginApiService, profile:Profile) {
        super($q,loginApi,profile);
        this.canEditProfile = false;
    }

    public navigateToUserWishes():void {
        this.$state.go('wishes',{userId:this.$stateParams['userId']});
    }

    public navigateToUserOfferts():void {
        this.$state.go('offerts',{userId:this.$stateParams['userId']});
    }
}

class ProfileDetailConfig {

    public static $inject:Array<string> = ['$stateProvider'];

    constructor($stateProvider:angular.ui.IStateProvider) {
        $stateProvider
            .state('myProfile', {
                parent: 'home',
                url: '/myProfile',
                templateUrl: ProfileDetailTpl,
                controller: MyProfileDetailCtrl,
                controllerAs: 'ctrl'
            })
            .state('profile', {
                parent: 'home',
                url: '/profiles/:userId',
                templateUrl: ProfileDetailTpl,
                controller: UserProfileDetailCtrl,
                controllerAs: 'ctrl',
                resolve: {
                    profile: ['$stateParams','profilesApi',userProfileResolve]
                }
            })
    }
}

function userProfileResolve($stateParams:angular.ui.IStateParamsService, profilesApi:ProfilesApiService):angular.IPromise<Profile> {
    return profilesApi.query($stateParams['userId']);
}
