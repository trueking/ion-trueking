import {Profile} from "../../models/profile.models";
import {ProfilesApiService} from "../../services/profiles-api.service";
let WishesTpl = require('./wishes.tpl.html');

export default class WishesViewModule {

    private ngWishesViewModule:angular.IModule;

    constructor() {
        this.ngWishesViewModule = angular.module(WishesViewModule.moduleName(), [])
            .config(WishesConfig);
    }

    public static moduleName():string {
        return 'trueking.profiles.views.wishes';
    }

    public getModule():angular.IModule {
        return this.ngWishesViewModule;
    }
}

abstract class WishesCtrl {
    public canEditWishes:boolean;
    constructor(protected profile:Profile) {}
}

class MyWishesCtrl extends WishesCtrl{
    public static $inject:Array<string> = ['$state','profilesApi','myProfile'];
    constructor(private $state:angular.ui.IStateService, private profilesApi:ProfilesApiService, private myProfile:Profile) {
        super(myProfile);
        this.canEditWishes = true;
    }

    public selectTag():void {
        this.profilesApi.updateMyTags(this.myProfile.wishedTags)
            .then(()=>{
                this.$state.reload();
            });
    }
}

class UserWishesCtrl extends WishesCtrl{
    public static $inject:Array<string> = ['profile'];
    constructor(profile:Profile) {
        super(profile);
        this.canEditWishes = false;
    }
}

class WishesConfig {

    public static $inject:Array<string> = ['$stateProvider'];

    constructor($stateProvider:angular.ui.IStateProvider) {
        $stateProvider
            .state('myWishes', {
                parent: 'home',
                url: '/myWishes',
                templateUrl: WishesTpl,
                controller: MyWishesCtrl,
                controllerAs: 'ctrl'
            })
            .state('wishes', {
                parent: 'home',
                url: '/profiles/:userId/wishes',
                templateUrl: WishesTpl,
                controller: UserWishesCtrl,
                controllerAs: 'ctrl',
                resolve: {
                    profile: ['$stateParams','profilesApi',userProfileResolve]
                }
            })
    }
}

function userProfileResolve($stateParams:angular.ui.IStateParamsService, profilesApi:ProfilesApiService):angular.IPromise<Profile> {
    return profilesApi.query($stateParams['userId']);
}