import './../css/profiles.css';
import ProfileDetailViewModule from "./profile-detail/profile-detail";
import WishesViewModule from "./wishes/wishes";

export default class ProfilesViewsModule {
    private ngProfilesViewsModule: angular.IModule;
    constructor(){
        this.ngProfilesViewsModule = angular.module(ProfilesViewsModule.moduleName(),[
            new ProfileDetailViewModule().getModule().name,
            new WishesViewModule().getModule().name
        ]);
    }

    public static moduleName():string {
        return 'trueking.profiles.views';
    }

    public getModule():angular.IModule {
        return this.ngProfilesViewsModule;
    }
}