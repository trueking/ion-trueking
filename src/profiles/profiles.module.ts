import ProfilesViewsModule from "./views/profiles.views.module";
import ProfilesServicesModule from "./services/profiles.services.module";

export default class ProfilesModule {
    private ngProfilesModule: angular.IModule;
    constructor(){
        this.ngProfilesModule = angular.module(ProfilesModule.moduleName(),[
            new ProfilesServicesModule().getModule().name,
            new ProfilesViewsModule().getModule().name
        ]);
    }

    public static moduleName():string {
        return 'trueking.profiles';
    }

    public getModule():angular.IModule {
        return this.ngProfilesModule;
    }
}