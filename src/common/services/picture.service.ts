import {ToastServiceModule,ToastService} from "./toast.service";
import {CloudImagesService} from "./cloudImages.service";

export class PictureServiceModule {
    private ngPictureServiceModule:angular.IModule;

    constructor() {
        this.ngPictureServiceModule = angular.module(PictureServiceModule.moduleName(), [
            'ngCordova',
			ToastServiceModule.moduleName()
        ])
            .service('pictureService', PictureService)
    }

    public static moduleName():string {
        return 'trueking.common.services.picture';
    }

    public getModule():angular.IModule {
        return this.ngPictureServiceModule;
    }

}

export class PictureService {
    public static $inject:Array<string> = ['$q','$cordovaCamera','toastService','cloudImagesService'];

    constructor(private $q:angular.IQService, private $cordovaCamera:any, private toastService:ToastService,
                private cloudImagesService:CloudImagesService) {}

    private defaultOptions():any {
        return {
            quality: 100,
            destinationType: 0, //DATA_URL
            mediaType: 0, //PICTURE
            encodingType: 0, //JPEG
            targetWidth: 640,
            targetHeight: 480,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
    }

    private takePicture(options:any):angular.IPromise<string> {
        let defer:angular.IDeferred<string> = this.$q.defer();

        this.$cordovaCamera.getPicture(options)
            .then((imageData:string)=> {
                defer.resolve(imageData);
            }, (errorMsg:string)=> {
				this.toastService.showShortBottom(errorMsg);
                defer.reject(errorMsg);
            });

        return defer.promise;
    }

    public takePictureAndUploadToCloud(captureOtions?:any):angular.IPromise<string> {
        return this.takePicture(captureOtions)
            .then((imageData:string)=>{
                return this.cloudImagesService.uploadImage(imageData);
            });
    }

    public takePictureFromCamera(captureOtions?:any):angular.IPromise<string> {
        let options = angular.extend({}, this.defaultOptions(),captureOtions);
        options.sourceType =  1; //CAMERA
        return this.takePictureAndUploadToCloud(options);
    }

    public takePictureFromGallery(captureOtions?:any):angular.IPromise<string> {
        let options = angular.extend({}, this.defaultOptions(),captureOtions);
        options.sourceType =  0; //PHOTOLIBRARY
        return this.takePictureAndUploadToCloud(options);
    }

}
