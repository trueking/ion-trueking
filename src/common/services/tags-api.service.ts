import {RestApiServiceModule} from "./rest-api.service";
import {RestApiService} from "./rest-api.service";

export class TagsApiServiceModule {
    private ngTagsApiServiceModule:angular.IModule;

    constructor() {
        this.ngTagsApiServiceModule = angular.module(TagsApiServiceModule.moduleName(), [
            RestApiServiceModule.moduleName()
        ])
            .service('tagsApi', TagsApiService)
    }

    public static moduleName():string {
        return 'trueking.common.services.tags-api';
    }

    public getModule():angular.IModule {
        return this.ngTagsApiServiceModule;
    }
}

export class TagsApiService {
    public static $inject:Array<string> = ['restApi'];
    public tagsEndpoint:string = '/tags';

    constructor(private restApi:RestApiService) {}

    public query(search:string):ng.IPromise<Array<string>> {
        return this.restApi.get(this.tagsEndpoint + '/autocomplete/' + search, null, null, null, true, false)
            .then((resp:any)=> {
                return resp.data;
            });
    }

}