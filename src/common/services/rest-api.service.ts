import {LoadingService, LoadingServiceModule} from "./loading.service";
import {LocalStorageService} from "./localstorage.service";
import {LoginService} from "../../login/services/login.service";

export class RestApiServiceModule {
    private ngRestApiServiceModule:angular.IModule;

    constructor() {
        this.ngRestApiServiceModule = angular.module(RestApiServiceModule.moduleName(), [
            'angular-cache',
            LoadingServiceModule.moduleName()
        ])
            .config(RestApiConfig)
            .service('restApi', RestApiService);
    };

    public static moduleName():string {
        return 'trueking.common.services.rest-api';
    }

    public getModule():angular.IModule {
        return this.ngRestApiServiceModule;
    }

}


export class RestApiService {
    public static $inject:Array<string> = ['$http', '$q', 'CacheFactory', 'loadingService','localStorageService'];
    private restApiCache:any;
    private baseUrl:string = 'https://dry-plateau-50413.herokuapp.com';

    constructor(private $http:angular.IHttpService, private $q:angular.IQService,
                private CacheFactory:any, private loadingService:LoadingService, private localStorageService:LocalStorageService) {

        this.restApiCache = this.CacheFactory.createCache('restApiCache', {
            maxAge: 600000,
            deleteOnExpire: 'aggressive',
            capacity: 1000
        });
    }

    /**Cache methods**/

    public static encodeUriQuery(val:any, pctEncodeSpaces:boolean = false):string {
        return encodeURIComponent(val)
            .replace(/%40/gi, '@')
            .replace(/%3A/gi, ':')
            .replace(/%24/g, '$')
            .replace(/%2C/gi, ',')
            .replace(/%20/g, (pctEncodeSpaces ? '%20' : '+'));
    }

    public getRestApiCache():any{
        return angular.copy(this.restApiCache);
    }


    public invalidateUri(uri:string, params?:any, anyParam:Boolean=false, defaultBaseUri:boolean = true):void {
		if (defaultBaseUri) uri = this.baseUrl+uri;
        if (!params) this.restApiCache.remove(uri);
        else if (Object.keys(params).length === 0) this.restApiCache.remove(uri);
        else {
            let cacheUris:Array<string> = this.restApiCache.keys();
            cacheUris.forEach((cacheUri)=>{
                if (cacheUri.indexOf(uri.trim()+'?') > -1) {
                    let deleteUri:boolean = false;
                    for (let paramKey in params) {
                        deleteUri = (cacheUri.indexOf(paramKey + '=' + RestApiService.encodeUriQuery(params[paramKey])) > -1);
                        if (anyParam && deleteUri) break;
                        if (!anyParam && !deleteUri) break;
                    }
                    if (deleteUri === true) this.restApiCache.remove(cacheUri);
                }
            });
        }
    }

    public invalidateUriIgnoreParams(uri:string, defaultBaseUri:boolean = true):void {
		if (defaultBaseUri) uri = this.baseUrl+uri;
        let cacheUris:Array<string> = (<any>this.restApiCache).keys();
        cacheUris.map((cacheUri:string)=> {
            if (cacheUri.trim() === uri.trim()) {
                this.restApiCache.remove(cacheUri);
            }
            if (cacheUri.indexOf(uri+'?') > -1) {
                this.restApiCache.remove(cacheUri);
            }
        });
    }

    public invalidateUriAndSubUris(uri:string, defaultBaseUri:boolean = true):void {
        if (defaultBaseUri) uri = this.baseUrl+uri;
        let cacheUris:Array<string> = (<any>this.restApiCache).keys();
        cacheUris.map((cacheUri:string)=> {
            if (cacheUri.indexOf(uri) > -1) {
                this.restApiCache.remove(cacheUri);
            }
        });
    }

    public invalidateAllUris():void {
        this.restApiCache.removeAll();
    }

    /**RESTFULL methods**/

    private defaultHttpConfig():any {
        let credentialsToken = this.localStorageService.get('credentials-token');
        return {
            headers: {
                Authorization: (credentialsToken)? 'Basic ' + credentialsToken:null
            }
        };
    }

    private defaultErrorCB(showLoading:boolean, errorCB:Function):void {
        if (showLoading) this.loadingService.hideLoading();
        if (errorCB) errorCB();
    }

    private defaultSuccessCB(showLoading:boolean, successCB:Function):void {
        if (showLoading) this.loadingService.hideLoading();
        if (successCB) successCB();
    }

    public get(endpoint:string, params?:any, errorCB?:Function, successCB?:Function, aviodCache:boolean = false,
               showLoading:boolean = true, httpConfig:any = {}, baseUriIncluded:boolean = false):angular.IPromise<any> {
        if (showLoading) this.loadingService.showLoading();
        //Mount config object
        let defaultGetHttpConfig:any = {
            params: params,
            cache: aviodCache ? false : this.restApiCache
        };
        //Shallow copy of all the free configurations, precedence from right to left
        let config = angular.extend({}, this.defaultHttpConfig(), defaultGetHttpConfig, httpConfig);
        //Service call
        let apiUrl = baseUriIncluded ? endpoint: this.baseUrl + endpoint;
        return this.$http.get(apiUrl, config)
            .then((response:any)=> { //Success callback
                    this.defaultSuccessCB(showLoading, successCB);
                    return response;
                },
                (errorResponse:any)=> { // Error callback
                    this.defaultErrorCB(showLoading, errorCB);
                    return this.$q.reject(errorResponse);
                });
    }

    public post(endpoint:string, data:any, errorCB?:Function, successCB?:Function, showLoading:boolean = true,
                httpConfig:any = {}, baseUriIncluded:boolean = false):angular.IPromise<any> {
        if (showLoading) this.loadingService.showLoading();
        let defaultPostHttpConfig:any = {};
        //Shallow copy of all the free configurations, precedence from right to left
        let config:any = angular.extend({}, this.defaultHttpConfig(), defaultPostHttpConfig, httpConfig);
        //Service call
        let apiUrl = baseUriIncluded ? endpoint:this.baseUrl + endpoint;
        return this.$http.post(apiUrl, data, config)
            .then((response:any)=> { //Success callback
                    this.defaultSuccessCB(showLoading, successCB);
                    return response;
                },
                (errorResponse:any)=> { // Error callback
                    this.defaultErrorCB(showLoading, errorCB);
                    return this.$q.reject(errorResponse);
                });
    }

    public put(endpoint:string, data:any, errorCB?:Function, successCB?:Function, showLoading:boolean = true,
               httpConfig:any = {}, baseUriIncluded:boolean = false):angular.IPromise<any> {
        if (showLoading) this.loadingService.showLoading();
        let defaultPutHttpConfig:any = {};
        //Shallow copy of all the free configurations, precedence from right to left
        let config:any = angular.extend({}, this.defaultHttpConfig(), defaultPutHttpConfig, httpConfig);
        //Service call
        let apiUrl = baseUriIncluded ? endpoint:this.baseUrl + endpoint;
        return this.$http.put(apiUrl, data, config)
            .then((response:any)=> { //Success callback
                    this.defaultSuccessCB(showLoading, successCB);
                    return response;
                },
                (errorResponse:any)=> { // Error callback
                    this.defaultErrorCB(showLoading, errorCB);
                    return this.$q.reject(errorResponse);
                });

    }

    public delete(endpoint:string, params?:any, errorCB?:Function, successCB?:Function, showLoading:boolean = true,
                  httpConfig:any = {}, baseUriIncluded:boolean = false):angular.IPromise<any> {
        if (showLoading) this.loadingService.showLoading();
        //Mount config object
        let defaultDeleteHttpConfig:any = {
            params: params,
        };
        //Shallow copy of all the free configurations, precedence from right to left
        let config:any = angular.extend({}, this.defaultHttpConfig(), defaultDeleteHttpConfig, httpConfig);
        //Service call
        let apiUrl = baseUriIncluded ? endpoint:this.baseUrl + endpoint;
        return this.$http.delete(apiUrl, config)
            .then((response:any)=> { //Success callback
                    this.defaultSuccessCB(showLoading, successCB);
                    return response;
                },
                (errorResponse:any)=> { // Error callback
                    this.defaultErrorCB(showLoading, errorCB);
                    return this.$q.reject(errorResponse);
                });
    }
}

class RestApiConfig {
	public static $inject: Array<string> = ['$httpProvider'];
	constructor($httpProvider:angular.IHttpProvider) {
        //Default $http config
        //Headers
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/json';
        $httpProvider.defaults.headers.put['Content-Type'] = 'application/json';
        $httpProvider.defaults.headers.get = {
            'Cache-Control': 'no-cache',
            'Pragma': 'no-cache'
        };
        //Interceptors
		$httpProvider.interceptors.push(['$q','$injector',function($q:angular.IQService, $injector:any) {
            return {
                request: (config:any)=>{
                    config.timeout = 30000;
                    return config;
                },
                responseError: (rejection:any)=> {
                    let $ionicPopup:ionic.popup.IonicPopupService = $injector.get('$ionicPopup');
                    let loginService:LoginService = $injector.get('loginService');
                    if (rejection.status === 401) {
                        $ionicPopup.alert({
                            title: 'Error en la sessió!',
                            okType: 'button-assertive'
                        }).then(()=>{
                            loginService.logout();
                        });
                    } else {
                        $ionicPopup.alert({
                            title: 'S\'ha produit un error en el servidor',
                            okType: 'button-assertive'
                        });
                        return $q.reject(rejection);
                    }
                }
            }
        }]);
	}
}
