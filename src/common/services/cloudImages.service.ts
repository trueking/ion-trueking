import {RestApiService} from "./rest-api.service";
export class CloudImagesServiceModule {
    private ngCloudImagesServiceModule:angular.IModule;

    constructor() {
        this.ngCloudImagesServiceModule = angular.module(CloudImagesServiceModule.moduleName(), [])
            .service('cloudImagesService', CloudImagesService)
    }

    public static moduleName():string {
        return 'trueking.common.services.cloud-images';
    }

    public getModule():angular.IModule {
        return this.ngCloudImagesServiceModule;
    }

}

export class CloudImagesService {
    public static $inject:Array<string> = ['$q','restApi'];
    private clientID:string = '1dd5cbb454e2e86';
    private baseUri:string = 'https://api.imgur.com/3';
    constructor(private $q:angular.IQService, private restApi:RestApiService) {}

    public defaultConfig():any {
        return {
            headers: {
                Authorization: 'Client-ID ' + this.clientID
            }
        };
    }

    public uploadImages(base64Image:Array<string>):angular.IPromise<Array<string>> {
        let promises = [];
        for (let i=0; i < base64Image.length; ++i) {
            promises.push(this.uploadImage(base64Image[i]));
        }
        return this.$q.all(promises);
    }

    public uploadImage(base64Image:string):angular.IPromise<string> {
        return this.restApi.post(this.baseUri + '/upload',{image:base64Image, type:'base64'},null,null,true,this.defaultConfig(),true)
            .then((resp:any)=>{
                if (!resp.data.data) return this.$q.reject(resp.data);
                return resp.data.data.link;
            },(errResp:any)=>{
                return this.$q.reject(errResp);
            });
    }
}