import {LoadingService} from "./loading.service";
export class FileServiceModule {
    private ngFileServiceModule:angular.IModule;

    constructor() {
        this.ngFileServiceModule = angular.module(FileServiceModule.moduleName(), [])
            .service('fileService', FileService)
    }

    public static moduleName():string {
        return 'trueking.common.services.file';
    }

    public getModule():angular.IModule {
        return this.ngFileServiceModule;
    }

}

export class FileService {
    public static $inject:Array<string> = ['$q','loadingService'];

    constructor(private $q:angular.IQService, private loadingService:LoadingService) {}

    public pathsToBase64Files(imagePath:Array<string>):angular.IPromise<Array<string>> {
        var promises = [];
        for (let i=0; i<imagePath.length; ++i) {
            promises.push(this.pathToBase64File(imagePath[i]));
        }
        return this.$q.all(promises);
    }

    public pathToBase64File(imagePath):angular.IPromise<string> {
        var self = this;
        let defered:angular.IDeferred<string> = this.$q.defer();

        this.loadingService.showLoading();
        var xhr = new XMLHttpRequest();
        xhr.open('GET', imagePath, true);
        xhr.responseType = "blob";
        xhr.onload = function (e) {
            if (this.status !== 200) {
                self.loadingService.hideLoading();
                defered.reject(this.status);
                return;
            }
            var reader = new FileReader();
            reader.onload = function(event:any) {
                self.loadingService.hideLoading();
                defered.resolve(event.target.result.match(/^data:.+\/(.+);base64,(.*)$/)[2]);
            };
            var file = this.response;
            reader.readAsDataURL(file);
        };
        xhr.send();

        return defered.promise;
    }
}