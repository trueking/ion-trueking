export class LoadingServiceModule {
    private ngLoadingServiceModule:angular.IModule;

    constructor() {
        this.ngLoadingServiceModule = angular.module(LoadingServiceModule.moduleName(), [])
            .service('loadingService', LoadingService)
            .constant('$ionicLoadingConfig', {
                template: '<p class="item-icon-left">Working...<ion-spinner class="spinner-balanced" icon="ripple"/></p>',
                duration: 10000
            });
    }

    public static moduleName():string {
        return 'trueking.common.services.loading';
    }

    public getModule():angular.IModule {
        return this.ngLoadingServiceModule;
    }

}

export class LoadingService {
    public static $inject:Array<string> = ['$ionicLoading'];
    public loadingCalls:number;

    constructor(private $ionicLoading:ionic.loading.IonicLoadingService) {
        this.loadingCalls = 0;
    }

    public getLoadingCalls() {
        return this.loadingCalls;
    }

    public showLoading():void {
        if (++this.loadingCalls === 1) {
            this.$ionicLoading.show();
        }
    }

    public hideLoading():void {
        if (this.loadingCalls === 0) return;
        if (--this.loadingCalls === 0) {
            this.$ionicLoading.hide();
        }
    }
}

