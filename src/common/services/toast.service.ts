export class ToastServiceModule {
	private ngToastServiceModule:angular.IModule;

	constructor() {
		this.ngToastServiceModule = angular.module(ToastServiceModule.moduleName(), [])
			.service('toastService', ToastService)
	}

	public static moduleName():string {
		return 'trueking.common.services.toast';
	}

	public getModule():angular.IModule {
		return this.ngToastServiceModule;
	}
}

export class ToastService {
	public static $inject:Array<string> = ['$cordovaToast'];

	constructor(private $cordovaToast:any) {}

	private toastFactory(message:string, position:string, duration:string, color:string):any {

		switch(color) {
			case 'green':
				color = '#00A000';
				break;
			case 'red':
				color = '#cc0000';
				break;
		}

		this.$cordovaToast.showWithOptions({
			message: message,
			duration: duration,
			position: position,
			styling: {
				opacity: 0.75, // 0.0 (transparent) to 1.0 (opaque). Default 0.8
				backgroundColor: color, // make sure you use #RRGGBB. Default #333333
				textColor: '#FFFF00', // Ditto. Default #FFFFFF
				textSize: 20.5, // Default is approx. 13.
				cornerRadius: 50, // minimum is 0 (square). iOS default 20, Android default 100
				horizontalPadding: 20, // iOS default 16, Android default 50
				verticalPadding: 16 // iOS default 12, Android default 30
			}
		});
	}

	//Normal

	public showShortTop(message:string):void {
		this.$cordovaToast.showShortTop(message);
	}

	public showShortCenter(message:string):void {
		this.$cordovaToast.showShortCenter(message);
	}

	public showShortBottom(message:string):void {
		this.$cordovaToast.showShortBottom(message);
	}

	public showLongTop(message:string):void {
		this.$cordovaToast.showLongTop(message);
	}

	public showLongCenter(message:string):void {
		this.$cordovaToast.showLongCenter(message);
	}

	public showLongBottom(message:string):void {
		this.$cordovaToast.showLongBottom(message);
	}

	//Succes

	public showSuccesShortTop(message:string):void {
		this.toastFactory(message,'top','short','green');
	}

	public showSuccesShortCenter(message:string):void {
		this.toastFactory(message,'center','short','green');
	}

	public showSuccesShortBottom(message:string):void {
		this.toastFactory(message,'bottom','short','green');
	}

	public showSuccesLongTop(message:string):void {
		this.toastFactory(message,'top','long','green');
	}

	public showSuccesLongCenter(message:string):void {
		this.toastFactory(message,'center','long','green');
	}

	public showSuccesLongBottom(message:string):void {
		this.toastFactory(message,'bottom','long','green');
	}

	//Error

	public showErrorShortTop(message:string):void {
		this.toastFactory(message,'top','short','red');
	}

	public showErrorShortCenter(message:string):void {
		this.toastFactory(message,'center','short','red');
	}

	public showErrorShortBottom(message:string):void {
		this.toastFactory(message,'bottom','short','red');
	}

	public showErrorLongTop(message:string):void {
		this.toastFactory(message,'top','long','red');
	}

	public showErrorLongCenter(message:string):void {
		this.toastFactory(message,'center','long','red');
	}

	public showErrorLongBottom(message:string):void {
		this.toastFactory(message,'bottom','long','red');
	}
}
