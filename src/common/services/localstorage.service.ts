export class LocalStorageServiceModule {
    private ngLocalStorageServiceModule:angular.IModule;

    constructor() {
        this.ngLocalStorageServiceModule = angular.module(LocalStorageServiceModule.moduleName(), [
                'angular-cache'
        ]).service('localStorageService', LocalStorageService)
    }

    public static moduleName():string {
        return 'trueking.common.services.localstorage';
    }

    public getModule():angular.IModule {
        return this.ngLocalStorageServiceModule;
    }

}

export class LocalStorageService {
    public static $inject:Array<string> = ['CacheFactory'];
    private localStorageCache:any;

    constructor(private CacheFactory:any) {
        this.localStorageCache = this.CacheFactory.createCache('localStorageCache', {
            storageMode: 'localStorage'
        });
    }

    public get(key:string):any {
        return this.localStorageCache.get(key);
    }

    public put(key:string,value:any):void {
        this.localStorageCache.put(key,value);
    }

    public remove(key:string):void {
        this.localStorageCache.remove(key);
    }
}
