import {LoadingServiceModule} from './loading.service';
import {ToastServiceModule} from "./toast.service";
import {RestApiServiceModule} from './rest-api.service';
import {PictureServiceModule} from "./picture.service";
import {LocalStorageServiceModule} from "./localstorage.service";
import {TagsApiServiceModule} from "./tags-api.service";
import {FileServiceModule} from "./file.service";
import {CloudImagesServiceModule} from "./cloudImages.service";

export default class CommonServicesModule {
	private ngCommonServicesModule:angular.IModule;

	constructor() {
		this.ngCommonServicesModule = angular.module(CommonServicesModule.moduleName(), [
			new LoadingServiceModule().getModule().name,
			new ToastServiceModule().getModule().name,
			new RestApiServiceModule().getModule().name,
			new TagsApiServiceModule().getModule().name,
			new PictureServiceModule().getModule().name,
			new LocalStorageServiceModule().getModule().name,
			new FileServiceModule().getModule().name,
			new CloudImagesServiceModule().getModule().name
		]);
	}

	public static moduleName():string {
		return 'trueking.common.services';
	}

	public getModule():angular.IModule {
		return this.ngCommonServicesModule;
	}
}
