let TruekingLogoImage = require('./../../assets/pictures/trueking_title_logo.png');

export default class NavTitleDirectiveModule {

    private ngNavTitleDirectiveModule:angular.IModule;

    constructor() {
        this.ngNavTitleDirectiveModule = angular.module(NavTitleDirectiveModule.moduleName(), [])
            .directive('truekingLogoTitle', ()=>new NavTitleDirective());
    }

    public static moduleName():string {
        return 'trueking.common.components.nav-title';
    }

    public getModule():angular.IModule {
        return this.ngNavTitleDirectiveModule;
    }
}

class NavTitleDirective implements angular.IDirective {
    public template:string = '<div><span style="vertical-align:middle"><img style="padding-top: 5px" ng-src="{{ctrl.truekingLogoImage}}"><ng-transclude></ng-transclude></span></div>';
    public restrict:string = 'E';
    public transclude:boolean = true;
    public replace:boolean = true;
    public controller:any = NavTitleCtrl;
    public controllerAs:string = 'ctrl';

    constructor() {
    }
}

class NavTitleCtrl {
    public truekingLogoImage:string;

    constructor() {
        this.truekingLogoImage = TruekingLogoImage;
    }
}

