import NavTitleDirectiveModule from './nav-title/nav-title.directive';
import GalleryImageDirectiveModule from "./gallery-image/gallery-image.directive";
import ImagesGalleryDirectiveModule from "./images-gallery/images-gallery";
import TagsManagerDirectiveModule from "./tags-manager/tags-manager.directive";
import ImageDetailDirectiveModule from "./image-detail/image-detail";
import TkItemCardDirectiveModule from "./tk-item-card/tk-item-card.directive";
import TkItemCardListDirectiveModule from "./tk-item-card-list/tk-item-card-list.directive";

export default class CommonComponentsModule {
    private ngCommonComponentsModule:angular.IModule;

    constructor() {
        this.ngCommonComponentsModule = angular.module(CommonComponentsModule.moduleName(), [
            new NavTitleDirectiveModule().getModule().name,
            new GalleryImageDirectiveModule().getModule().name,
            new ImagesGalleryDirectiveModule().getModule().name,
            new TagsManagerDirectiveModule().getModule().name,
            new ImageDetailDirectiveModule().getModule().name,
            new TkItemCardDirectiveModule().getModule().name,
            new TkItemCardListDirectiveModule().getModule().name
        ]);
    }

    public static moduleName():string {
        return 'trueking.common.components';
    }

    public getModule():angular.IModule {
        return this.ngCommonComponentsModule;
    }
}