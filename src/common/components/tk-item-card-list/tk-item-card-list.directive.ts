import {Item} from "../../../items/models/item.models";
let TkItemCardListTpl = require('./tk-item-card-list.tpl.html');

export default class TkItemCardListDirectiveModule {

    private ngTkItemCardListDirectiveModule:angular.IModule;

    constructor() {
        this.ngTkItemCardListDirectiveModule = angular.module(TkItemCardListDirectiveModule.moduleName(), [])
            .directive('tkItemCardList', ()=>new TkItemCardListDirective());
    }

    public static moduleName():string {
        return 'trueking.common.components.tk-item-card-list';
    }

    public getModule():angular.IModule {
        return this.ngTkItemCardListDirectiveModule;
    }
}

class TkItemCardListCtrl {
    public items:Array<Item>;
    public myProfileId:number;
    constructor(){}
}

class TkItemCardListDirective implements angular.IDirective {
    public templateUrl:string = TkItemCardListTpl;
    public restrict:string = 'E';
    public controller:any = TkItemCardListCtrl;
    public controllerAs:string = 'ctrl';
    public scope:any = {};
    public bindToController:any = {
        items:'=ngModel',
        myProfileId:'=',
		multiselect:'='
    };

    constructor() {}
}
