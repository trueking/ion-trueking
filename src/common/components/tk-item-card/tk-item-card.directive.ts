import {Item} from "../../../items/models/item.models";
let TkItemCardTpl = require('./tk-item-card.tpl.html');
let ImageNotFoundPicture = require('./../../assets/pictures/image_not_found.jpg');

export default class TkItemCardDirectiveModule {

    private ngTkItemCardDirectiveModule:angular.IModule;

    constructor() {
        this.ngTkItemCardDirectiveModule = angular.module(TkItemCardDirectiveModule.moduleName(), [])
            .directive('tkItemCard', ()=>new TkItemCardDirective());
    }

    public static moduleName():string {
        return 'trueking.common.components.tk-item-card';
    }

    public getModule():angular.IModule {
        return this.ngTkItemCardDirectiveModule;
    }
}

class TkItemCardCtrl {
    public static $inject:Array<string> = ['$state'];
    public item:Item;
    public myProfileId:number;
    public imageNotFound:any = ImageNotFoundPicture;
    constructor(private $state:angular.ui.IStateService){}

    public navigateToItem():void {
        let state:string;
        if (this.item.owner == this.myProfileId) state = 'editItem';
        else state = 'viewItem';
        this.$state.go(state,{itemId:this.item.id});
    }
}

class TkItemCardDirective implements angular.IDirective {
    public templateUrl:string = TkItemCardTpl;
    public restrict:string = 'E';
    public controller:any = TkItemCardCtrl;
    public controllerAs:string = 'ctrl';
    public scope:any = {};
    public bindToController:any = {
        item:'=ngModel',
        myProfileId: '=',
		selectable:'='
    };

    constructor() {}
}
