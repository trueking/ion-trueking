let GalleryImageTpl = require('./gallery-image.tpl.html');

export default class GalleryImageDirectiveModule {

    private ngGalleryImageDirectiveModule:angular.IModule;

    constructor() {
        this.ngGalleryImageDirectiveModule = angular.module(GalleryImageDirectiveModule.moduleName(), [])
            .directive('galleryImage', ()=>new GalleryImageDirective());
    }

    public static moduleName():string {
        return 'trueking.common.components.gallery-image';
    }

    public getModule():angular.IModule {
        return this.ngGalleryImageDirectiveModule;
    }
}

class GalleryImageDirective implements angular.IDirective {
    public templateUrl:string = GalleryImageTpl;
    public restrict:string = 'AE';
    public replace:boolean = true;
    public controller:any = GalleryImageCtrl;
    public controllerAs:string = 'ctrl';
    public scope:any = {};
    public bindToController:any = {
        src: '='
    };

    constructor() {}
}

class GalleryImageCtrl {
    constructor() {}
}