import {TagsApiService} from "../../services/tags-api.service";
let TagsManagerTpl = require('./tags-manager.tpl.html');
let AddTagsModalTpl = require('./add-tags-modal.tpl.html');

export default class TagsManagerDirectiveModule {

    private ngTagsManagerDirectiveModule:angular.IModule;

    constructor() {
        this.ngTagsManagerDirectiveModule = angular.module(TagsManagerDirectiveModule.moduleName(), [])
            .directive('tagsManager', ()=>new TagsManagerDirective());
    }

    public static moduleName():string {
        return 'trueking.common.components.tags-manager';
    }

    public getModule():angular.IModule {
        return this.ngTagsManagerDirectiveModule;
    }
}

class TagsManagerCtrl {
    public static $inject = ['$scope', '$ionicModal','tagsApi'];

    public disabled:boolean;
    public tags:Array<string>;
    public onSelectFn:Function;
    public onDeleteFn:Function;
    public serviceTags:Array<string>;
    public search:string;
    public searching:boolean;
    public wishes:Array<string>;
    public addTagsModal:ionic.modal.IonicModalController;

    constructor(private $scope:any, private $ionicModal:ionic.modal.IonicModalService, private tagsApi:TagsApiService){
        this.searching=false;
        this.serviceTags = [];

        this.$scope.$on('$destroy',()=>{
            if (this.addTagsModal) this.closeModal();
        });
    }

    public addTag():void {
        let ionicModalOptions:ionic.modal.IonicModalOptions = {
            scope:this.$scope,
            focusFirstInput:true
        };
        this.$ionicModal.fromTemplateUrl(AddTagsModalTpl,ionicModalOptions)
            .then((addTagsModal:any)=>{
                this.addTagsModal = addTagsModal;
                this.addTagsModal.show();
            });
    }

    public deleteTag(index:number) {
        this.tags.splice(index,1);
        if (this.onDeleteFn) this.onDeleteFn();
    }

    public loadWishlist():void {
        if (this.wishes && this.wishes.length) {
            this.tags = angular.copy(this.wishes);
        }
    }

    //Modal functions

    public closeModal():void {
        this.search = null;
        this.serviceTags = null;
        this.addTagsModal.remove();
    }

    public searchTags():void {
        if (this.search) {
            this.serviceTags = [];
            this.searching = true;
            this.tagsApi.query(this.search)
                .then((serviceTags:Array<string>)=>{
                    this.serviceTags = serviceTags.filter((tag)=>{
                        return (this.tags.indexOf(tag) == -1);
                    });
                    this.searching = false;
                },(errResp:any)=>{
                    this.searching = false;
                });
        }
    }

    public selectTag(tag:string):void {
        if (tag) {
            this.tags.push(tag);
            this.closeModal();
            if (this.onSelectFn) this.onSelectFn();
        }
    }
}

class TagsManagerDirective implements angular.IDirective {
    public templateUrl:string = TagsManagerTpl;
    public restrict:string = 'E';
    public controller:any = TagsManagerCtrl;
    public controllerAs:string = 'ctrl';
    public scope:any = {};
    public bindToController:any = {
        tags: '=ngModel',
        disabled:'=?ngDisabled',
        listTitle:'@?',
        onSelectFn:'&?onSelect',
        onDeleteFn:'&?onDelete',
        wishes:'='
    };

    constructor() {}
}
