let ImageDetailTpl = require('./image-detail.tpl.html');
let ImageDetailModalTpl = require('./image-detail-modal.tpl.html');

export default class ImageDetailDirectiveModule {

    private ngImageDetailDirectiveModule:angular.IModule;

    constructor() {
        this.ngImageDetailDirectiveModule = angular.module(ImageDetailDirectiveModule.moduleName(), [])
            .directive('imageDetail', ()=>new ImageDetailDirective());
    }

    public static moduleName():string {
        return 'trueking.common.components.image-detail';
    }

    public getModule():angular.IModule {
        return this.ngImageDetailDirectiveModule;
    }
}

class ImageDetailDirective implements angular.IDirective {
    public templateUrl:string = ImageDetailTpl;
    public restrict:string = 'A';
    public replace:boolean = true;
    public controller:any = ImageDetailCtrl;
    public controllerAs:string = 'ctrl';
    public scope:any = {};
    public bindToController:any = {
        image: '=imageDetail'
    };

    constructor() {}
}

class ImageDetailCtrl {
    public static $inject:Array<string> = ['$scope','$ionicModal'];
    public image:string;
    public detailModal:ionic.modal.IonicModalController;

    constructor(private $scope:angular.IScope, private $ionicModal:ionic.modal.IonicModalService) {
        //Destroy detailModal
        this.$scope.$on('$destroy',()=> {
            if (this.detailModal) this.detailModal.remove();
        });
    }

    public openDetailModal():void {
        let modalOptions:ionic.modal.IonicModalOptions = {
            scope: this.$scope
        };
        this.$ionicModal.fromTemplateUrl(ImageDetailModalTpl,modalOptions)
            .then((modal:ionic.modal.IonicModalController)=> {
                this.detailModal = modal;
                this.detailModal.show();
            });
    }

    public closeDetailModal():void {
        this.detailModal.remove();
    }
}