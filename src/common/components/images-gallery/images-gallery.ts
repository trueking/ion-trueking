let ImagesGalleryTpl = require('./images-gallery.tpl.html');
let ImagesGalleryDetailTpl = require('./images-gallery-detail.tpl.html');
let ImagePopoverTpl = require('./image-options-popover.tpl.html');

export default class ImagesGalleryDirectiveModule {

    private ngImagesGalleryDirectiveModule:angular.IModule;

    constructor() {
        this.ngImagesGalleryDirectiveModule = angular.module(ImagesGalleryDirectiveModule.moduleName(), [])
            .directive('imagesGallery', ()=>new ImagesGalleryDirective());
    }

    public static moduleName():string {
        return 'trueking.common.components.images-gallery';
    }

    public getModule():angular.IModule {
        return this.ngImagesGalleryDirectiveModule;
    }
}

class ImagesGalleryDirective implements angular.IDirective {
    public templateUrl:string = ImagesGalleryTpl;
    public restrict:string = 'AE';
    public replace:boolean = true;
    public controller:any = ImagesGalleryCtrl;
    public controllerAs:string = 'ctrl';
    public scope:any = {};
    public bindToController:any = {
        images: '=ngModel',
        disabled:'=ngDisabled'
    };

    constructor() {}
}

class ImagesGalleryCtrl {

    public static $inject = ['$scope','$ionicModal','$ionicPopover'];
    public images:Array<string>;
    public disabled:boolean;
    public detailModal:ionic.modal.IonicModalController;
    public imagePopover:ionic.popover.IonicPopoverController;
    public imageIndex:number;

    constructor(private $scope:any, private $ionicModal:ionic.modal.IonicModalService, private $ionicPopover:ionic.popover.IonicPopoverService) {
        //Init popover
        this.$ionicPopover.fromTemplateUrl(ImagePopoverTpl, {
            scope:this.$scope
        }).then((popover)=>{
            this.imagePopover = popover;
        });

        //Destroy detailModal and imagePopover when free directive
        this.$scope.$on('$destroy',()=> {
            if (this.detailModal) this.detailModal.remove();
            if (this.imagePopover)this.imagePopover.remove();
        });
    }

    public removePictureFromGallery(index:number):void {
        this.images.splice(index,1);
        this.imagePopover.hide();
    }

    public showRemoveImagePopover(event,index):void {
        if (!this.disabled) {
            this.imageIndex = index;
            this.imagePopover.show(event);
        }
    }

    public showImagesDetail(index):void {
        this.imageIndex = index;
        this.$ionicModal.fromTemplateUrl(ImagesGalleryDetailTpl, {
            scope: this.$scope,
            animation: 'slide-in-up'
        }).then((modal:ionic.modal.IonicModalController)=>{
            this.detailModal = modal;
            this.detailModal.show();
        });
    }

    public closeImagesDetailModal() {
        this.detailModal.remove();
    }
}