import EqualsValidatorDirectiveModule from "./equals-validator.directive";
import AsyncValidatorDirectiveModule from "./async-validator";

export default class CommonUtilsModule {
    private ngCommonUtilsModule:angular.IModule;

    constructor() {
        this.ngCommonUtilsModule = angular.module(CommonUtilsModule.moduleName(), [
            new EqualsValidatorDirectiveModule().getModule().name,
            new AsyncValidatorDirectiveModule().getModule().name
        ]);
    }

    public static moduleName():string {
        return 'trueking.common.utils';
    }

    public getModule():angular.IModule {
        return this.ngCommonUtilsModule;
    }
}