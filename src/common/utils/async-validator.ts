export default class AsyncValidatorDirectiveModule {

    private nAsyncValidatorDirectiveModule:angular.IModule;

    constructor() {
        this.nAsyncValidatorDirectiveModule = angular.module(AsyncValidatorDirectiveModule.moduleName(), [])
            .directive('asyncValidator', ()=>new AsyncValidatorDirective());
    }

    public static moduleName():string {
        return 'trueking.common.utils.async-boolean-validator';
    }

    public getModule():angular.IModule {
        return this.nAsyncValidatorDirectiveModule;
    }
}

class AsyncValidatorDirective implements angular.IDirective {
    public require:Array<string> = ['ngModel','asyncValidator'];
    public scope:any = {};
    public controller:any = AsyncValidatorCtrl;
    public controllerAs:string = 'ctrl';
    public bindToController:any = {
        asyncBooleanFn: '&asyncValidator'
    };

    public link:angular.IDirectiveLinkFn = function(scope:angular.IScope, elem:angular.IAugmentedJQuery,attr:angular.IAttributes,requiredCtrls:Array<any>) {
		let ngModelCtrl:angular.INgModelController;
		let myCtrl:AsyncValidatorCtrl;
		[ngModelCtrl,myCtrl] = requiredCtrls;
		ngModelCtrl.$asyncValidators['asyncValidator'] = (modelValue, viewValue)=>{
			return myCtrl.asyncBooleanFn({data:modelValue});
		}
    };

    constructor() {}
}

class AsyncValidatorCtrl {
	public asyncBooleanFn:Function;
    constructor() {}
}