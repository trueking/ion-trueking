import IDirectiveLinkFn = angular.IDirectiveLinkFn;
export default class EqualsValidatorDirectiveModule {

    private ngEqualsValidatorDirectiveModule:angular.IModule;

    constructor() {
        this.ngEqualsValidatorDirectiveModule = angular.module(EqualsValidatorDirectiveModule.moduleName(), [])
            .directive('equals', ()=>new EqualsValidatorDirective());
    }

    public static moduleName():string {
        return 'trueking.common.utils.equals-validator';
    }

    public getModule():angular.IModule {
        return this.ngEqualsValidatorDirectiveModule;
    }
}

class EqualsValidatorDirective implements angular.IDirective {
    public require:Array<string> = ['ngModel','equals'];
    public scope:any = {};
	public controller:any = EqualsValidatorCtrl;
	public controllerAs:string = 'ctrl';
	public bindToController:any = {
		otherValue: '=equals'
	};
    public link:IDirectiveLinkFn = function(scope:angular.IScope, elem:angular.IAugmentedJQuery,attr:angular.IAttributes,requiredCtrls:Array<any>) {
		let ngModelCtrl:angular.INgModelController;
		let myCtrl:EqualsValidatorCtrl;
		[ngModelCtrl,myCtrl] = requiredCtrls;
		ngModelCtrl.$validators['equals'] = (modelValue:any, viewValue:any):boolean=> {
            return (modelValue === myCtrl.otherValue);
        };
        scope.$watch(():any=>{return myCtrl.otherValue},():void=> {
            ngModelCtrl.$validate();
        });
    };

    constructor() {}
}

class EqualsValidatorCtrl {
	public otherValue:any;
	constructor(){}
}