export class Self {
    public href;

    constructor(json?:any) {
        if (!json) json = {};
        this.href = json.href;
    }

}

export class Credentials {
    public self: Self;
    public userName:string;
    public password: string;

    constructor(json?:any) {
        if(!json) json = {};
        this.self = json.self || new Self();
        this.userName = json.userName;
    }
}

export class ServiceError {
    public errorCode:string;
    public message:string;
    public args:Array<string>;

    constructor(json?:any) {
        this.errorCode = json.errorCode;
        this.message = json.message;
        this.args = json.args;
    }
}