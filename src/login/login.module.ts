import LoginViewsModule from "./views/login.views.module";
import LoginServicesModule from "./services/login.services.module";

export default class LoginModule {
	private ngLoginModule: angular.IModule;
	constructor(){
		this.ngLoginModule = angular.module(LoginModule.moduleName(),[
			new LoginServicesModule().getModule().name,
			new LoginViewsModule().getModule().name
		]);
	}

	public static moduleName():string {
		return 'trueking.login';
	}

	public getModule():angular.IModule {
		return this.ngLoginModule;
	}
}
