import {Self} from "../../common/models/common.models";
import {Credentials} from "../../common/models/common.models";

export class UserAccountInfo {
    public self: Self;
    public name:string;
    public surnames: string;
    public email:string;
    public zipCode:string;
    public credentials: Credentials;

    constructor(json?:any) {
        if (!json) json = {};
        this.name = json.name;
        this.surnames = json.surnames;
        this.email = json.email;
        this.zipCode = json.zipCode;
        this.credentials = json.credentials || new Credentials();
    }
}