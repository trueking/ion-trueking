import {LocalStorageService} from "../../common/services/localstorage.service";
import {Credentials} from "../../common/models/common.models";
import {LoginApiService} from "./login-api.service";
import {RestApiService} from "../../common/services/rest-api.service";
let CryptoJS = require("./../../../bower_components/crypto-js/crypto-js.js");

export class LoginServiceModule {
    private ngLoginServiceModule:angular.IModule;

    constructor() {
        this.ngLoginServiceModule = angular.module(LoginServiceModule.moduleName(), [])
            .service('loginService', LoginService);
    };

    public static moduleName():string {
        return 'trueking.login.services.login';
    }

    public getModule():angular.IModule {
        return this.ngLoginServiceModule;
    }

}

export class LoginService {
    public static $inject = ['$q','$state','$ionicPopup','$ionicHistory','localStorageService','loginApi','restApi'];
    constructor(private $q:angular.IQService, private $state:angular.ui.IStateService, private $ionicPopup:ionic.popup.IonicPopupService,
                private $ionicHistory:ionic.navigation.IonicHistoryService, private localStorageService:LocalStorageService,
                private loginApi:LoginApiService, private restApi:RestApiService) {}

    public cleanIonicNavCache():angular.IPromise<any> {
        let defered:angular.IDeferred<any> = this.$q.defer();
        this.restApi.invalidateAllUris();
        this.$ionicHistory.clearHistory();
        this.$ionicHistory.clearCache()
            .then(()=>{
                defered.resolve();
            },()=>{
                this.$ionicPopup.alert({
                    title: 'Error intern!',
                    okType: 'button-assertive'
                }).then(ionic.Platform.exitApp);
            });
        let nextViewOptions:ionic.navigation.IonicHistoryNextViewOptions = {
            historyRoot: true
        };
        this.$ionicHistory.nextViewOptions(nextViewOptions);
        return defered.promise;
    }

    public login(credentials?:Credentials,destination:string = 'items'):angular.IPromise<boolean> {
        let authToken:string;
        if (credentials) authToken = btoa(credentials.userName + ':' + CryptoJS.MD5(credentials.password).toString());
        else {
            let authHeader:string =  this.localStorageService.get('credentials-token');
            authToken = (authHeader)? authHeader.replace('Basic','').trim():null;
        }

        this.localStorageService.remove('credentials-token');
        return this.loginApi.login(authToken)
            .then((isAccepted:boolean)=>{
                if (isAccepted) { //Login succes
                    this.localStorageService.put('credentials-token',authToken);
                    this.cleanIonicNavCache()
                        .then(()=> {
                            this.$state.go(destination);
                        });
                }
                return isAccepted;
            });
    }

    public logout() {
        this.localStorageService.remove('credentials-token');
        this.cleanIonicNavCache()
            .then(()=> {
                this.$state.go('loginWelcome');
            });
    }

}