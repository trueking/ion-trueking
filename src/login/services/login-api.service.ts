import {RestApiServiceModule,RestApiService} from "../../common/services/rest-api.service";
import {UserAccountInfo} from "../models/account.models";
import {Credentials} from "../../common/models/common.models";
import {LoginService} from "./login.service";
import {ToastService} from "../../common/services/toast.service";

export class LoginApiServiceModule {
    private ngLoginApiServiceModule:angular.IModule;

    constructor() {
        this.ngLoginApiServiceModule = angular.module(LoginApiServiceModule.moduleName(), [
                RestApiServiceModule.moduleName()
            ])
            .service('loginApi', LoginApiService);
    };

    public static moduleName():string {
        return 'trueking.login.services.login-api';
    }

    public getModule():angular.IModule {
        return this.ngLoginApiServiceModule;
    }

}

export class LoginApiService {
    public static $inject = ['$q', 'restApi'];
    public accountsEndpoint:string;
    constructor(private $q:angular.IQService, private restApi:RestApiService) {
        this.accountsEndpoint = '/accounts';
    }

    public create(userAccount:UserAccountInfo):angular.IPromise<any>  {
        return this.restApi.post(this.accountsEndpoint,userAccount);
    }

    public resetPassword(email:string):angular.IPromise<boolean> {
        return this.restApi.put(this.accountsEndpoint + '/resetPassword', email)
            .then((resp:any)=> {
                return resp.data;
            });
    }

    public login(authToken:string):angular.IPromise<boolean> {
        return this.restApi.post(this.accountsEndpoint + '/login',{token:authToken})
            .then((resp:any)=>{
                return resp.data;
            });
    }

    private checkAsyncValidation(validationUri:string):angular.IPromise<boolean> {

        return this.restApi.get(this.accountsEndpoint + validationUri, null, null, null, true, false)
            .then((resp:any)=>{
                let isAvailable:boolean = resp.data;
                if (isAvailable) return this.$q.reject(isAvailable);
            });
    }

    public checkUserNameAvailable(userName:string):angular.IPromise<boolean> {
        return this.checkAsyncValidation('/existUserName/' + userName);
    }

    public checkEmailAvailable(email:string):angular.IPromise<boolean> {
        return this.checkAsyncValidation('/existEmail/' + email);
    }
}
