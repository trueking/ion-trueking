import {LoginApiServiceModule} from "./login-api.service";
import {LoginServiceModule} from "./login.service";

export default class LoginServicesModule {

    private ngLoginServicesModule:angular.IModule;

    constructor() {
        this.ngLoginServicesModule = angular.module(LoginServicesModule.moduleName(), [
            new LoginApiServiceModule().getModule().name,
            new LoginServiceModule().getModule().name
        ]);
    }

    public static moduleName():string {
        return 'trueking.login.services';
    }

    public getModule():angular.IModule {
        return this.ngLoginServicesModule;
    }
}