import {UserAccountInfo} from "../../models/account.models";
import {LoginApiService} from "../../services/login-api.service";
import {ToastService} from "../../../common/services/toast.service";
let SignUpTpl = require('./sign-up.tpl.html');

export default class SignUpViewModule {

    private ngSignUpViewModule:angular.IModule;

    constructor() {
        this.ngSignUpViewModule = angular.module(SignUpViewModule.moduleName(), [])
            .config(SignUpConfig);
    }

    public static moduleName():string {
        return 'trueking.login.views.sign-up';
    }

    public getModule():angular.IModule {
        return this.ngSignUpViewModule;
    }
}

class SignUpCtrl {

	public static $inject:Array<string> = ['$state', '$ionicPopup','loginApi', 'toastService', 'userAccountInfo'];

    constructor(private $state:angular.ui.IStateService, private $ionicPopup:ionic.popup.IonicPopupService, private loginApi:LoginApiService,private toastService:ToastService,
                public userAccountInfo:UserAccountInfo) {}

    public createAccount():void {
        this.loginApi.create(this.userAccountInfo)
            .then((Resp)=>{
                this.toastService.showShortBottom('Compte creat amb èxit');
                this.userAccountInfo.credentials.userName = null;
                this.userAccountInfo.email = null;
                this.$state.go('signIn');
            },(errResp)=>{
                this.$ionicPopup.alert({
                    title: 'S\'ha produït un error amb el servidor',
                    okType: 'button-assertive'
                }).then(()=>{
                    this.userAccountInfo.credentials.userName = null;
                    this.userAccountInfo.email = null;
                });
            });
    }
}

class SignUpConfig {
    public static $inject:Array<string> = ['$stateProvider'];

    constructor($stateProvider:angular.ui.IStateProvider) {
        $stateProvider
            .state('signUp', {
                parent: 'login',
                url: '/sign-up',
                templateUrl: SignUpTpl,
                controller: SignUpCtrl,
                controllerAs: 'ctrl',
                resolve: {
                    userAccountInfo: resolveUserAccountInfo
                }
            });
    }
}

function resolveUserAccountInfo(): UserAccountInfo {
    return new UserAccountInfo();
}