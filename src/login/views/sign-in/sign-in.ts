import {LoginService} from "../../services/login.service";
import {Credentials} from "../../../common/models/common.models";
import {ToastService} from "../../../common/services/toast.service";
import {LoginApiService} from "../../services/login-api.service";
let SignInTpl = require('./sign-in.tpl.html');

export default class SignInViewModule {

    private ngSignInViewModule:angular.IModule;

    constructor() {
        this.ngSignInViewModule = angular.module(SignInViewModule.moduleName(), [])
            .config(SignInConfig);
    }

    public static moduleName():string {
        return 'trueking.login.views.sign-in';
    }

    public getModule():angular.IModule {
        return this.ngSignInViewModule;
    }
}

class SignInCtrl {
	public static $inject = ['$state','$ionicPopup','loginService','loginApi'];
    public credentials:Credentials;
    constructor(private $state:angular.ui.IStateService, private $ionicPopup:ionic.popup.IonicPopupService,
                private loginService:LoginService, private loginApi:LoginApiService) {
        this.credentials = new Credentials();
    }

	public forgotPasswordPopup() {
		this.$ionicPopup.prompt({
			title: 'Reset de contrasenya',
			subTitle: 'Inserta el correu electrònic enllaçat al teu compte trueking',
			inputType: 'email',
			cancelText: 'Cancel·lar'
		}).then((email:string)=>{
			if(email) {
                this.loginApi.resetPassword(email)
                    .then((success:boolean)=>{
                        if(success) {
                            this.$ionicPopup.alert({
                                title: 'S\'ha enviat un email a ' + email,
                                okType: 'button-balanced'
                            });
                        } else {
                            this.$ionicPopup.alert({
                                title: 'El correu ' + email + ' es incorrecte.',
                                okType: 'button-assertive'
                            });
                        }
                    });
			}
			else if (email === "") {
				this.$ionicPopup.alert({
					title: 'Format de correu incorrecte',
					okType: 'button-assertive'
				});
			}
		});
	}

    public login():void {
        this.loginService.login(this.credentials)
            .then((accepted:boolean)=>{
                if (!accepted) this.$ionicPopup.alert({
                    title: 'Usuari o contrasenya incorrecte',
                    okType: 'button-assertive'
                });
            });
    }
}

class SignInConfig {
    public static $inject:Array<string> = ['$stateProvider'];

    constructor($stateProvider:angular.ui.IStateProvider) {
        $stateProvider
            .state('signIn', {
                parent: 'login',
                url: '/sign-in',
                templateUrl: SignInTpl,
                controller: SignInCtrl,
                controllerAs: 'ctrl'
            })
    }
}