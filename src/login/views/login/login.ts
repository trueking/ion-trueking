let LoginTpl = require('./login.tpl.html');
let TruekingFullLogo = require('./../../../common/assets/pictures/logo_full.png');

export default class LoginViewModule {

    private ngLoginViewModule:angular.IModule;

    constructor() {
        this.ngLoginViewModule = angular.module(LoginViewModule.moduleName(), [])
            .config(LoginConfig);
    }

    public static moduleName():string {
        return 'trueking.login.views.login';
    }

    public getModule():angular.IModule {
        return this.ngLoginViewModule;
    }
}

class LoginCtrl {
    public truekingFullLogo:any;
    constructor(){
        this.truekingFullLogo = TruekingFullLogo;
    }
}

class LoginConfig {
    public static $inject:Array<string> = ['$stateProvider'];

    constructor($stateProvider:angular.ui.IStateProvider) {
        $stateProvider
            .state('loginWelcome', {
                parent: 'login',
                url: '/welcome',
                templateUrl: LoginTpl,
                controller: LoginCtrl,
                controllerAs: 'ctrl'
            })
    }
}