import './../css/login.css';
import LoginViewModule from "./login/login";
import SignInViewModule from "./sign-in/sign-in";
import SignUpViewModule from "./sign-up/sign-up";

export default class LoginViewsModule {
    private ngLoginViewsModule:angular.IModule;

    constructor() {
        this.ngLoginViewsModule = angular.module(LoginViewsModule.moduleName(), [
                new LoginViewModule().getModule().name,
                new SignInViewModule().getModule().name,
                new SignUpViewModule().getModule().name
            ])
            .config(LoginViewsConfig);
    }

    public static moduleName():string {
        return 'trueking.login.views';
    }

    public getModule():angular.IModule {
        return this.ngLoginViewsModule;
    }
}

class LoginViewsConfig {
    public static $inject:Array<string> = ['$stateProvider'];

    constructor($stateProvider:angular.ui.IStateProvider) {
        $stateProvider
            .state('login', {
                url: '/login',
                abstract: true,
                template:   '<ion-view class="login-bg">' +
                                '<ion-nav-bar class="bar-clear">' +
                                    '<ion-nav-back-button></ion-nav-back-button>' +
                                '</ion-nav-bar>' +
                                '<ion-nav-view></ion-nav-view>' +
                            '</ion-view>'
            });
    }
}
