import './home.css';
import {ProfilesApiService} from "../profiles/services/profiles-api.service";
import {Profile} from "../profiles/models/profile.models";
let HomeTpl = require('./home.tpl.html');
let pictureNotFoundImage = require('./../common/assets/pictures/image_not_found.jpg');

export default class HomeModule {
    private ngHomeModule: angular.IModule;
    constructor(){
        this.ngHomeModule = angular.module(HomeModule.moduleName(),[])
            .config(HomeConfig);
    }

    public static moduleName():string {
        return 'trueking.home';
    }

    public getModule():angular.IModule {
        return this.ngHomeModule;
    }
}

class HomeCtrl {
    public static $inject = ['$state','myProfile'];
    public profilePicture:any;

    constructor(private $state:angular.ui.IStateService, protected myProfile:Profile){
        this.profilePicture =  this.myProfile.profilePicture || pictureNotFoundImage;
    }

    public goToMyProfile():void {
        this.$state.go('myProfile');
    }
}

class HomeConfig {
    public static $inject:Array<string> = ['$stateProvider'];
    constructor($stateProvider:angular.ui.IStateProvider) {
        $stateProvider
            .state('home', {
                url:'/home',
                abstract: true,
                templateUrl: HomeTpl,
                controller: HomeCtrl,
                controllerAs: 'ctrl',
                resolve: {
                    myProfile:['profilesApi',(profilesApi:ProfilesApiService)=> profilesApi.queryMine()]
                }
            })
    }
}