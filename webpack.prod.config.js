//Imports
var webpack = require('webpack');
var path = require("path");
//Common webpack configuration
var commonConfig = require('./webpack.common.config');

var config = {
  entry: {
    app: commonConfig.appEntries,
    vendors: commonConfig.vendors
  },
  output: {
    path: commonConfig.outputPath,
    filename: 'trueking.bundle.min.js'
  },
  resolve: commonConfig.resolve,
  plugins: commonConfig.plugins.concat([
    new webpack.optimize.CommonsChunkPlugin('vendors', 'trueking.vendors.min.js'),
    new webpack.optimize.UglifyJsPlugin()
  ]),
  module: {
    loaders: commonConfig.loaders
  }
};

module.exports = config;
