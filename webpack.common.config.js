//Webpack common builds options
'use strict';

var webpack = require('webpack');
var path = require("path");
//Webpack plugins
var HtmlWebpackPlugin = require('html-webpack-plugin');

var _appEntries = [path.resolve(__dirname, 'src/main.ts')];
var _outputPath = path.resolve(__dirname, 'www/');

var _vendors = [
    //js
    path.resolve(__dirname, 'bower_components/ionic/js/ionic.bundle.js'),
    path.resolve(__dirname, 'bower_components/angular-cache/dist/angular-cache.js'),
    path.resolve(__dirname, 'bower_components/ngCordova/dist/ng-cordova.js'),
    //css
    //scss
    path.resolve(__dirname, 'src/common/scss/ionic.app.scss')
];

var _resolve = {
    extensions: ['', '.ts', '.js', '.css'],
    modulesDirectories: ['node_modules', 'bower_components']
};

var _loaders = [
    {test: /\.ts$/, loader: 'ts-loader'},
    {test: /\.css$/, loader: 'style!css'},
    {test: /\.scss$/, loaders: ['style', 'css', 'sass']},
    {test: /\.html$/, loader: 'ngtemplate?relativeTo=' + (path.resolve(__dirname, './src')) + '/!html'},
    {test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url-loader?limit=10000&mimetype=application/font-woff'},
    {test: /\.(jpg|png|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'file-loader'}
];

var _plugins = [
    new HtmlWebpackPlugin({
        title: 'Tuerking',
        template: 'src/index.ejs'
    }),
    new webpack.optimize.DedupePlugin()
];

module.exports = {
    appEntries: _appEntries,
    outputPath: _outputPath,
    vendors: _vendors,
    resolve: _resolve,
    loaders: _loaders,
    plugins: _plugins
};

