'use strict';

var webpackCommonConfig = require('../webpack.common.config');

var _basePath = '../';

var _files = [
    //Global vendors
    'bower_components/ionic/js/ionic.bundle.js',
    //Global test vendors
    'bower_components/angular-mocks/angular-mocks.js',
    //My tests
    'test/unit/**/*.unit.ts',
	//My app entry point
	'src/main.ts'
];

var _webpackPreprocessors = {
	'test/**/*.unit.ts': ['webpack'],
	'src/main.ts': ['webpack']
};

var _webpack = {
    noInfo: true,
    resolve: webpackCommonConfig.resolve,
    module: {
        loaders: webpackCommonConfig.loaders
    }
};

var _webpackMiddleware = {
    noInfo: true,
    stats: {
        color: true,
        chunkModules: false,
        modules: false
    }
};


module.exports = {
    basePath: _basePath,
    files: _files,
	webpackPreprocessors: _webpackPreprocessors,
    webpack: _webpack,
    webpackMiddleware: _webpackMiddleware
};

