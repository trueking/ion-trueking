import ItemManagePage from './itemManage.po';

describe('items suite',function():void {

    describe('User want to create a new item', function():void {
        var itemManagePage = new ItemManagePage();
        beforeEach(function():void {
            itemManagePage.get();
        });

        it('should navigate to items creation page', function():void {
            expect(browser.getCurrentUrl()).toContain('/#/home/items');
        });

    });

});
