var reporters = require('jasmine-reporters');

exports.config = {
    baseUrl:'http://localhost:8100',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    capabilities: {
        'browserName': 'chrome'
    },
    specs: ['build/e2e/*.e2e.js'],
    framework: 'jasmine2',
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 60000,
        invertGrep: false
    },
    allScriptsTimeout: 30000,
    onPrepare: function () {
        jasmine.getEnv().addReporter(new reporters.TerminalReporter({verbosity: 3, color: true, showStack: true}));
    }
};
