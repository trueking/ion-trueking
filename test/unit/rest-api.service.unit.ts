import {RestApiServiceModule,RestApiService} from "../../src/common/services/rest-api.service";

class restApiCacheMock {
    public data:any;

    constructor() {
        this.data = {
            'http://trueking.herokuapp.com/items/1': {someFakeData: 'foo'},
            'http://trueking.herokuapp.com/items/1?withImages=true&bar=true': {someFakeData: 'foo'},
            'http://trueking.herokuapp.com/items/1?bar=true': {someBigFakeData: 'foo'},
            'http://trueking.herokuapp.com/items': {someFakeData: 'bar'},
            'http://trueking.herokuapp.com/tags': {someFakeData: 'foo', moreFakeData: 'bar'},
            'http://trueking.herokuapp.com/tags/33': {mokimoki: 'greep'},
            'http://trueking.herokuapp.com/tags?related=false&bar=true&foo=test&max=100': {someFakeData: 'fooBar'}
        };
    }

    public remove(key:string):void {
        delete this.data[key];
    }

    public get(key:string):any {
        return this.data[key];
    }

    public keys():Array<string> {
        return Object.keys(this.data);
    }

    public count() {
        return Object.keys(this.data).length;
    }
}

describe('restApi service', function () {

    var restApi:RestApiService;

    beforeEach(function () {
        angular.module('angular-cache', []);
        angular.mock.module(RestApiServiceModule.moduleName(), function ($provide) {
            $provide.provider('CacheFactory', function () {
                this.$get = function () {
                    return {
                        createCache: function () {
                            return new restApiCacheMock();
                        },
                    };
                };
            });
            $provide.service('loadingService', function () {
            });
        });
    });

    beforeEach(angular.mock.inject(function (_restApi_) {
        restApi = _restApi_;
    }));

    describe('requests cache', function () {

        var cacheCount;

        beforeEach(function () {
            cacheCount = restApi.getRestApiCache().count();
        });

        describe('invalidateUri function', function () {

            describe('without params', function () {

                describe('without anyParam option', function () {
                    it('should invalidate a request uri', function () {
                        restApi.invalidateUri('http://trueking.herokuapp.com/items',undefined,false,false);
                        expect(restApi.getRestApiCache().get('http://trueking.herokuapp.com/items')).toBe(undefined);
                        expect(restApi.getRestApiCache().count()).toBe(cacheCount - 1);
                    });

                    it('should invalidate a request uri with empty params', function () {
                        restApi.invalidateUri('http://trueking.herokuapp.com/items', {},false,false);
                        expect(restApi.getRestApiCache().get('http://trueking.herokuapp.com/items')).toBe(undefined);
                        expect(restApi.getRestApiCache().count()).toBe(cacheCount - 1);
                    });
                });

                describe('with any anyParam option', function () {
                    it('should invalidate a request uri with empty params', function () {
                        restApi.invalidateUri('http://trueking.herokuapp.com/items', {}, true,false);
                        expect(restApi.getRestApiCache().get('http://trueking.herokuapp.com/items')).toBe(undefined);
                        expect(restApi.getRestApiCache().count()).toBe(cacheCount - 1);
                    });
                });

            });

            describe('with params', function () {

                describe('without anyParam option', function () {

                    it('should invalidate all request uris that contain all params', function () {
                        restApi.invalidateUri('http://trueking.herokuapp.com/tags', {
                            bar: true,
                            related: false,
                            max: 100,
                            foo: 'test'
                        },false,false);
                        expect(restApi.getRestApiCache().get('http://trueking.herokuapp.com/tags?related=false&bar=true&foo=test&max=100')).toBe(undefined);
                        expect(restApi.getRestApiCache().count()).toBe(cacheCount - 1);
                        restApi.invalidateUri('http://trueking.herokuapp.com/items/1', {
                            withImages: true
                        },false,false);
                        expect(restApi.getRestApiCache().get('http://trueking.herokuapp.com/items/1?withImages=true&bar=true')).toBe(undefined);
                        expect(restApi.getRestApiCache().count()).toBe(cacheCount - 2);
                    });

                    it('should\'t invalidate uris if any param don\'t match', function () {
                        restApi.invalidateUri('http://trueking.herokuapp.com/items/1', {
                            failVar: 'must fail'
                        },false,false);
                        restApi.invalidateUri('http://trueking.herokuapp.com/items/1', {
                            withImages: true,
                            bar: true,
                            failVar: 'must fail'
                        },false,false);

                        restApi.invalidateUri('http://trueking.herokuapp.com/items/1', {
                            bar: true,
                            failVar: 'must fail'
                        },false,false);
                        expect(restApi.getRestApiCache().count()).toBe(cacheCount);
                    });

                });

                describe('with anyParam option', function () {

                    it('should invalidate all request uris that contain any parm passed', function () {
                        restApi.invalidateUri('http://trueking.herokuapp.com/items/1', {
                            bar: true,
                            failVar: 'must fail'
                        }, true,false);

                        expect(restApi.getRestApiCache().get('http://trueking.herokuapp.com/items/1?withImages=true&bar=true')).toBe(undefined);
                        expect(restApi.getRestApiCache().get('http://trueking.herokuapp.com/items/1?bar=true')).toBe(undefined);
                        expect(restApi.getRestApiCache().count()).toBe(cacheCount - 2);
                    });

                    it('should\'t invalidate any uri if non params passed are included', function () {
                        restApi.invalidateUri('http://trueking.herokuapp.com/items/1', {
                            failVar: 'must fail'
                        }, true,false);
                        expect(restApi.getRestApiCache().count()).toBe(cacheCount);
                    });

                });

            });

        });

        describe('invalidateUriIgnoreParamsFunction', function () {

            it('should invalidate all request uris that point to a concrete endpoint with and without params', function () {
                restApi.invalidateUriIgnoreParams('http://trueking.herokuapp.com/tags',false);
                expect(restApi.getRestApiCache().get('http://trueking.herokuapp.com/tags')).toBe(undefined);
                expect(restApi.getRestApiCache().get('http://trueking.herokuapp.com/tags?related=false&bar=true&foo=test&max=100')).toBe(undefined);
                expect(restApi.getRestApiCache().count()).toBe(cacheCount - 2);
            });

        });

    });

});