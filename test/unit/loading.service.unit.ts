import {LoadingServiceModule,LoadingService} from "../../src/common/services/loading.service";

describe('loading service', function() {

    var loadingService:LoadingService;
    var $ionicLoadingMock:any;

    beforeEach(function() {
        angular.mock.module(function($provide) {
            $provide.service('$ionicLoading', function () {
                this.show = jasmine.createSpy('showSpy').and.callThrough();
                this.hide = jasmine.createSpy('hideSpy').and.callThrough();
            });
        });
        angular.mock.module(LoadingServiceModule.moduleName());
    });

    beforeEach(angular.mock.inject(function(_loadingService_, $ionicLoading) {
        loadingService = _loadingService_;
        $ionicLoadingMock = $ionicLoading;
    }));

    describe('when loading is not displayed and call to display loading', function() {

        it('should call ionicLoader show func', function() {
            loadingService.showLoading();
            expect($ionicLoadingMock.show).toHaveBeenCalled();
        });

        it('should increment the number of times that service was invoked', function() {
            let timesBeforeCall = loadingService.getLoadingCalls();
            loadingService.showLoading();
            expect(loadingService.getLoadingCalls()).toBe(timesBeforeCall + 1);
            loadingService.showLoading();
            loadingService.showLoading();
            expect(loadingService.getLoadingCalls()).toBe(timesBeforeCall + 3);
        });

    });

    describe('when loading is displayed and call to display loading', function() {

        beforeEach(function() {
            loadingService.showLoading();
            $ionicLoadingMock.show.calls.reset();
        });

        it('shouldn\'t call ionicLoader show func', function() {
            loadingService.showLoading();
            expect($ionicLoadingMock.show).not.toHaveBeenCalled();
        });
    });

    describe('when loading is displayed and call to hide Loading', function() {

        beforeEach(function() {
            loadingService.showLoading();
        });

        it('shouldn\'t call ionicLoader hide func if there is some other process loading things', function() {
            loadingService.showLoading();
            loadingService.hideLoading();
            expect($ionicLoadingMock.hide).not.toHaveBeenCalled();
        });

        it('should call ionicLoader hide func if there is just one process loading things', function() {
            loadingService.hideLoading();
            expect($ionicLoadingMock.hide).toHaveBeenCalled();
        });

        it('should decrement the number of times that service was invoked', function() {
            loadingService.showLoading();
            loadingService.showLoading();
            let timesBeforeCall = loadingService.getLoadingCalls();
            loadingService.hideLoading();
            expect(loadingService.getLoadingCalls()).toBe(timesBeforeCall - 1);
            loadingService.hideLoading();
            loadingService.hideLoading();
            expect(loadingService.getLoadingCalls()).toBe(timesBeforeCall - 3);
        });

    });

    describe('when loading is not displayed and call to hide loading', function() {

        beforeEach(function() {
            loadingService.hideLoading();
        });

        it('shouldn\'t call ionicLoader hide func', function() {
            expect($ionicLoadingMock.show).not.toHaveBeenCalled();
        });

        it('shouldn\'t decrement the number of times that service was invoked. Min value = 0', function() {
            loadingService.hideLoading();
            loadingService.hideLoading();
            expect(loadingService.getLoadingCalls()).toBe(0);
        });

    });

});