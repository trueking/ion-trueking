'use strict';

var commonConfig = require('./karma.common.conf');

module.exports = function(config){
	config.set({

		basePath: commonConfig.basePath,

		preprocessors: commonConfig.webpackPreprocessors,

		webpack: commonConfig.webpack,

		webpackMiddleware: commonConfig.webpackMiddleware,

		files: commonConfig.files,

		colors: true,

		autoWatch: false,

		singleRun: true,

		frameworks: ['jasmine'],

		browsers: ['Chrome','Firefox'],

		reporters: ['progress', 'coverage', 'junit'],

		coverageReporter: {
			type : 'html',
			dir : 'test/reports/coverage'
		},

		junitReporter: {
			outputDir: 'test/reports/junit',
			outputFile: 'junit-report',
			suite: 'unit'
		}

	});
};
