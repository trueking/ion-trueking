'use strict';

var commonConfig = require('./karma.common.conf');

module.exports = function (config) {
	config.set({

		basePath: commonConfig.basePath,

		preprocessors: commonConfig.webpackPreprocessors,

		webpack: commonConfig.webpack,

		webpackMiddleware: commonConfig.webpackMiddleware,

		files: commonConfig.files,

		colors: true,

		autoWatch: true,

		singleRun: false,

		frameworks: ['jasmine'],

		browsers: ['Chrome']

	});
};
